/*
Copyright (c) 2011 Rdio Inc

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

// a global variable that will hold a reference to the api swf once it has loaded


var playlistPosition;
var playlistKeys;
var playlistText;
var model_input_batch,model_input_weights;

function startDataCollection(callback) {
  // console.log('Status height start ' + $('#status').height())
  var current_playlist;
  // console.log(playlistKeys)
  var data = {'token':$('#token').val(),'word1':$('#word1').val(),'word2':$('#word2').val(),'word3':$('#word3').val(),'model_input_batch':model_input_batch,
    'model_input_weights':model_input_weights,'playlistKeys':playlistKeys.join()};
  // console.log(playlistKeys)
  $.post( "/refresh", data, function( response, status, xhr ) {
    if ( status == "error" ) {
        var msg = "Sorry but there was an error: ";
    $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
  }


  // var status_height = $('#status').height()
  // $('#status').height(status_height)

  new_song = $.parseJSON(response)
  playlistText.push(new_song['text'])

    $("#loader").fadeOut("slow", function () {
        var playlist_songs = '';
        for (var i=0;i<playlistText.length;i++)
          {
            playlist_songs += '<div class="playlistSong" value="' + i + '">'
            playlist_songs += playlistText[i]

            if (i < playlist_length - 1){
            playlist_songs += '</div><hr class="playlist">'
            }
            else{
              playlist_songs += '</div>'
            }
          }
          // playlistText.join('</div><hr class="playlist">') + '<hr class="playlist">');
          $("#status").html(playlist_songs)
        $("#status").fadeIn("normal", function (){
            callback(new_song['trackKey']);

        });


    });

    model_input_batch = new_song['model_input_batch']
    model_input_weights = new_song['model_input_weights']

    playlistKeys.push(new_song['trackKey'])
    // if (playlistKeys.length == 1){
    //   apiswf.rdio_play(new_song['trackKey'])
    // }
    // apiswf.rdio_queue(new_song['trackKey'])

  });
}


$(function() {
  var availableTags = [];

    var csvRequest = new Request({
                    url:"/static/data/vocab_07112014.txt",
                    onSuccess:function(response){
                        //The response text is available in the 'response' variable
                        //Set the value of the textarea with the id 'csvResponse' to the response
                        all_vocab = response.split('\n');
                        for (var i=0; i < all_vocab.length; i++) {
                          // console.log(this.states[i].split(','))
                          availableTags.push(all_vocab[i].split(',')[0])
                        }
                    }
                }).send(); //Don't forget to send our request!

    console.log(availableTags)


    $( "#word1" ).autocomplete({
      source: availableTags
    });
    $( "#word2" ).autocomplete({
      source: availableTags
    });
    $( "#word3" ).autocomplete({
      source: availableTags
    });

  });




var apiswf = null;

$(document).ready(function() {

  playlistPosition = 0;

  playlistKeys = new Array();
  playlistText = new Array();
  model_input_batch = '0,0,0,0,0,0,0,0,0'
  model_input_weights = '0,0,0,0,0,0,1,1,1'

  // on page load use SWFObject to load the API swf into div#apiswf
  var flashvars = {
    'playbackToken': playback_token, // from token.js
    'domain': domain,                // from token.js
    'listener': 'callback_object'    // the global name of the object that will receive callbacks from the SWF
    };
  var params = {
    'allowScriptAccess': 'always'
  };
  var attributes = {};
  swfobject.embedSWF('http://www.rdio.com/api/swf/', // the location of the Rdio Playback API SWF
      'apiswf', // the ID of the element that will be replaced with the SWF
      1, 1, '9.0.0', 'expressInstall.swf', flashvars, params, attributes);


  // set up the controls
  $('#play').click(function() {
    // var track = $.parseJSON($('#playlist').val())
    // console.log(track[0])
    // apiswf.rdio_play(track[0]);
    apiswf.rdio_play();
  });
  $('#stop').click(function() { apiswf.rdio_stop(); });
  $('#pause').click(function() { apiswf.rdio_pause(); });

  $('#previous').click(function() {
    if (playlistPosition == 0){return}
    playlistPosition -= 1;
    apiswf.rdio_play(playlistKeys[playlistPosition]);
  });

  $('#next').click(function() {
    console.log('playlistPosition',playlistPosition)
    console.log(playlistKeys)
    console.log('playlist Length',playlistKeys.length)
    if (playlistPosition == (playlistKeys.length -1)){return}
    playlistPosition += 1;
    apiswf.rdio_play(playlistKeys[playlistPosition]);
  });

  $('#generate').click(function() {
      $("#loader").text('Loading...')
      $("#loader").fadeIn("slow");

      startDataCollection2(function(x){
        // console.log('onecallback'+x)

      });


  });



});

$(document).on('click', ".playlistSong", function() {

  console.log('Playing song...')
  playlistPosition = $(this).attr('value')
  console.log(playlistKeys[playlistPosition])
  apiswf.rdio_play(playlistKeys[playlistPosition]);

});


var current_playlist_length = 0;

var playlist_length = 1;


function startDataCollection2(callback) {

      $("#loader").fadeIn("slow");

      startDataCollection(function(trackKey){
        current_playlist_length += 1;
        if (current_playlist_length < playlist_length) {
          // console.log(trackKey);
          // console.log(current_playlist_length)
          startDataCollection2(function(current_playlist_length){

            if (current_playlist_length == playlist_length){
              console.log('done generating...')
            }
            else{
              // console.log('Fading in loader....')
              $("#loader").fadeIn("slow");
            }
          });

        }
        callback(current_playlist_length);
      });


}

// the global callback object
var callback_object = {};

callback_object.ready = function ready(user) {
  // Called once the API SWF has loaded and is ready to accept method calls.

  // find the embed/object element
  apiswf = $('#apiswf').get(0);

  apiswf.rdio_startFrequencyAnalyzer({
    frequencies: '10-band',
    period: 100
  });

  if (user == null) {
    $('#nobody').show();
  } else if (user.isSubscriber) {
    $('#subscriber').show();
  } else if (user.isTrial) {
    $('#trial').show();
  } else if (user.isFree) {
    $('#remaining').text(user.freeRemaining);
    $('#free').show();
  } else {
    $('#nobody').show();
  }

  // console.log(user);
}

callback_object.freeRemainingChanged = function freeRemainingChanged(remaining) {
  $('#remaining').text(remaining);
}

callback_object.playStateChanged = function playStateChanged(playState) {
  // The playback state has changed.
  // The state can be: 0 - paused, 1 - playing, 2 - stopped, 3 - buffering or 4 - paused.
  $('#playState').text(playState);
  console.log("Play state: ",playState)
}

callback_object.playingTrackChanged = function playingTrackChanged(playingTrack, sourcePosition) {
  // The currently playing track has changed.
  // Track metadata is provided as playingTrack and the position within the playing source as sourcePosition.
  console.log('sourcePosition',sourcePosition)
  console.log('playingTrack',playingTrack)

  // playlistPosition += 1;
  // apiswf.rdio_play(playlistKeys[playlistPosition]);

  if (playingTrack != null) {
    $('#track').text(playingTrack['name']);
    $('#album').text(playingTrack['album']);
    $('#artist').text(playingTrack['artist']);
    $('#art').attr('src', playingTrack['icon']);
  }
}

callback_object.playingSourceChanged = function playingSourceChanged(playingSource) {
  // The currently playing source changed.
  // The source metadata, including a track listing is inside playingSource.
}

callback_object.volumeChanged = function volumeChanged(volume) {
  // The volume changed to volume, a number between 0 and 1.
}

callback_object.muteChanged = function muteChanged(mute) {
  // Mute was changed. mute will either be true (for muting enabled) or false (for muting disabled).
}

callback_object.positionChanged = function positionChanged(position) {
  //The position within the track changed to position seconds.
  // This happens both in response to a seek and during playback.
  $('#position').text(position);
}

callback_object.queueChanged = function queueChanged(newQueue) {
  // The queue has changed to newQueue.
  console.log('queue changed')
  // console.log("number of playlists",playlistKeys.length)

  // apiswf.rdio_stop();
}

callback_object.shuffleChanged = function shuffleChanged(shuffle) {
  // The shuffle mode has changed.
  // shuffle is a boolean, true for shuffle, false for normal playback order.
}

callback_object.repeatChanged = function repeatChanged(repeatMode) {
  // The repeat mode change.
  // repeatMode will be one of: 0: no-repeat, 1: track-repeat or 2: whole-source-repeat.
}

callback_object.playingSomewhereElse = function playingSomewhereElse() {
  // An Rdio user can only play from one location at a time.
  // If playback begins somewhere else then playback will stop and this callback will be called.
}

callback_object.updateFrequencyData = function updateFrequencyData(arrayAsString) {
  // Called with frequency information after apiswf.rdio_startFrequencyAnalyzer(options) is called.
  // arrayAsString is a list of comma separated floats.

  var arr = arrayAsString.split(',');

  $('#freq div').each(function(i) {
    $(this).width(parseInt(parseFloat(arr[i])*500));
  })
}
