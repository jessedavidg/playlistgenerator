
/**
 * Provides suggestions for state names (USA).
 * @class
 * @scope public
 */



function StateSuggestions() {

    // console.log(this);
    // var placeholder = this;
    // console.log(this);
    $.get("/static/data/vocab_tiny.txt", $.proxy(function(data) {
      this.states = data.split('\n');
      for (var i=0; i < this.states.length; i++) {
        // console.log(this.states[i].split(','))
        this.states[i] = this.states[i].split(',')[0]
      }
      // console.log(this)
     },this));

      // console.log(this.states);  
    // this.states = tags;
    // console.log(x);
    // this.states = [
    //     "Alabama", "Alaska", "Arizona", "Arkansas",
    //     "California", "Colorado", "Connecticut",
    //     "Delaware", "Florida", "Georgia", "Hawaii",
    //     "Idaho", "Illinois", "Indiana", "Iowa",
    //     "Kansas", "Kentucky", "Louisiana",
    //     "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", 
    //     "Mississippi", "Missouri", "Montana",
    //     "Nebraska", "Nevada", "New Hampshire", "New Mexico", "New York",
    //     "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", 
    //     "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota",
    //     "Tennessee", "Texas", "Utah", "Vermont", "Virginia", 
    //     "Washington", "West Virginia", "Wisconsin", "Wyoming"  
    // ];

}

/**
 * Request suggestions for the given autosuggest control. 
 * @scope protected
 * @param oAutoSuggestControl The autosuggest control to provide suggestions for.
 */
StateSuggestions.prototype.requestSuggestions = function (oAutoSuggestControl /*:AutoSuggestControl*/,
                                                          bTypeAhead /*:boolean*/) {
    var aSuggestions = [];
    var sTextboxValue = oAutoSuggestControl.textbox.value;
    
    if (sTextboxValue.length > 0){
    
        //search for matching states
        for (var i=0; i < this.states.length; i++) { 
            if (this.states[i].indexOf(sTextboxValue) == 0) {
                aSuggestions.push(this.states[i]);
            } 
        }
    }

    //provide suggestions to the control
    oAutoSuggestControl.autosuggest(aSuggestions, bTypeAhead);
};