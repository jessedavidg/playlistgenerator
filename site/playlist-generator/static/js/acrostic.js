var songCache = { };

var currentMessage;
var currentGenre;
var currentSongs;
var maxTries = 100;
var curSong = null;


function error(s) {
    $("#info").addClass("alert-error");
    $("#info").removeClass("alert-success");
    $("#info").show();
    $("#info").text(s);
}

function info(s) {
    $("#info").removeClass("alert-error");
    $("#info").addClass("alert-success");
    $("#info").show();
    $("#info").text(s);
}

function clearError() {
    $("#info").hide("");
}

function getRdioID(song) {
    if (song && 'tracks' in song && song.tracks.length > 0) {
        var id = song.tracks[0].foreign_id;
        var rawID = id.split(':')[2]
        return rawID;
    } else {
        return null;
    }
}

function initUI() {
    $("#results").hide();

    $("#message").change(function() {
        go();
    });

    $("#save").click(function() {
        savePlaylist();
    });

    $("#go").click(function() {
        go();
    });

    $("#playNext").click(function() {
        playNextSong();
    });
    $("#playPrev").click(function() {
        playPrevSong();
    });

    $("#playPlay").click(function() {
        if (curSong == null) {
            if (currentSongs && currentSongs.length > 0) {
                playSong(currentSongs[0]);
            }
        } else {
            R.player.togglePause();
        }
    });

    $("#playPause").click(function() {
        R.player.togglePause();
    }).hide();
}

function go() {
    var msg = $("#message").val();
    var genre = $("#genre").val();

    if (msg.length == 0) {
        error("You must enter a secret message first.");
    } else if (genre.length == 0) {
        error("You must pick a genre");
    } else {
        generateAcrostic(genre, msg);
    }
}

function generateAcrostic(genre, msg) {
    clearError();
    curSong = null;
    currentMessage = normalizeMessage(msg);
    currentGenre = genre;
    createSongTable();
}


function createSongTable() {
    $("#results").show();
    var rows = $("#trows");
    rows.empty();
    currentSongs = [];

    _.each(currentMessage, function(c, i) {
        var row = $("<tr>");
        var char = $("<td class='char'>");
        char.html('<b>' + c.toUpperCase() + '</b>');
        var title = $("<td>");
        if (c == ' ') {
            title.html('&nbsp;');
        } else {
           var song = getMatchingSong(c, currentGenre);
            if (song) {
                console.log(song)
                song.prev = null;
                song.next = null;
                if (currentSongs.length > 0) {
                    var last = currentSongs[currentSongs.length - 1];
                    last.next = song;
                    song.prev = last;
                } 
                currentSongs.push(song);
                title.text(song.title + ' by ' + song.artist_name);
                song.row = row;
                row.on("click", function() {
                    playSong(song);
                });
            }
        }
        row.append(char);
        row.append(title);
        rows.append(row);
    });
}


function getMatchingSong(c, genre) {
    var gcache = songCache[c, genre];
    if (c in gcache) {
        var songList = gcache[c];
        if (songList.length > 0) {
            var song = songList.shift();
            songList.push(song);
            return song;
        }
    } else {
        return getMatchingSong(c, 'rock');
    }
    return null;
}


function normalizeMessage(msg) {
    var msg_array = [];

    _.each(msg, function(c, i) {
        c = c.toLowerCase();
        if (c.match(/[a-z]/)) {
            msg_array.push(c)
        } else if (msg_array.length > 0 && msg_array[msg_array.length -1] != ' ') {
            msg_array.push(' ');
        }
    });

    return msg_array;
}


function playSong(song) {
    if (song === curSong) {
        R.player.togglePause();
    } else {
        var oldSong = curSong;
        curSong = song;
        songChanged(oldSong);
        var id = getRdioID(curSong);
        if (id == null) {
            playNextSong();
        } else {
            console.log(typeof id)
            R.player.play({source: id});
        }
    }
}

function songChanged(song) {
    if (song) {
        if (song === curSong && R.player.playState() === R.player.PLAYSTATE_PLAYING) {
            song.row.addClass('success');
        } else {
            song.row.removeClass('success');
        }
    }
}

function playNextSong() {
    if (curSong) {
        if (curSong.next) {
            playSong(curSong.next);
        } 
    }  else {
        if (currentSongs && currentSongs.length > 0) {
            playSong(currentSongs[0]);
        }
    }
}

function playPrevSong() {
    if (curSong) {
        if (curSong.prev) {
            playSong(curSong.prev);
        } 
    }  else {
        if (currentSongs && currentSongs.length > 0) {
            playSong(currentSongs[0]);
        }
    }
}

function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

function fetchGenres() {
    var predefinedGenres = ['rock', 'country', 'pop', 'jazz', 'electronic', 'classical', 'hip hop', 
    'metal', 'r&b', 'blues', 'folk', 'experimental', 'alternative rock'];
    var defaultGenre = 'rock';

    predefinedGenres.sort();
    var glist = $("#genre");
    _.each(predefinedGenres, function(genre) {
        var url = '/static/js/genres/' + genre + '.js';
        $.getJSON(url, function(data) {
            songCache[genre] = {};

            _.each(data, function(song) {
                var letter = song.title[0].toLowerCase();
                if (! (letter in songCache[genre])) {
                    songCache[genre][letter] = [];
                }
                songCache[genre][letter].push(song);
            });
            _.each(songCache[genre], function(list, key) {
                shuffle(list);
            });
            var opt = $("<option>").attr('value', genre).text(genre);
            if (genre === defaultGenre) {
                opt.attr('selected', 'selected');
            }
            glist.append(opt);
        });
    });
}

function savePlaylist() {
    info("Saving the playlist");
    if (!R.authenticated()) {
        R.authenticate(
            function(state) {
                if (!state) {
                    error("Can't save the playlist unless you authorize this app");
                } else {
                    savePlaylistToRdio();
                }
            }
        );
    } else {
        savePlaylistToRdio();
    }
}

function getCurRdioTracks() {
    var ids = [];
    _.each(currentSongs, function(song) {
        var id = getRdioID(song);
        if (id) {
            ids.push(id);
        }
    });
    return ids;
}

function savePlaylistToRdio() {
    var ids = getCurRdioTracks();
    var msg = $("#message").val();
    var description = 'Created by The Acrostic Playlist Maker';
    var title = msg;

    if (ids.length > 0) {
        R.request( 
        { 
            method:"createPlaylist", 
            content: { 
                name: title,
                description: description,
                tracks: ids 
            },

            success: function(response) {
                info("Playlist titled '" + title + "' has been saved to Rdio.");
            },

            error: function(response) {
                error("whoops - had some trouble saving that playlist to rdio");
            }
        });
    }  else {
    }
}

$(document).ready(function() {
    $.ajaxSetup( {cache: false});
    initUI();
    fetchGenres();
    R.ready(
        function() {
            R.player.on("change:playingTrack", function(track) {
                if (track === null) {
                    playNextSong();
                }
            });
            
            R.player.on("change:playState", function(state) {
                if (state == R.player.PLAYSTATE_PAUSED) {
                    $("#playPlay").show();
                    $("#playPause").hide();
                }
                if (state == R.player.PLAYSTATE_PLAYING) {
                    $("#playPlay").hide();
                    $("#playPause").show();
                }

                if (state == R.player.PLAYSTATE_STOPPED) {
                    // playNextSong();
                }
                songChanged(curSong);
            });

            R.player.on("change:playingSource", function(track) {
                songChanged(curSong);
            });

        }
        );
    });