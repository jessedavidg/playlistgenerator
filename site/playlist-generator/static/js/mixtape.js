var songCache = { };

var currentMessage;
var currentGenre;
var currentSongs = [];
var playlistKeys = [];
var playlistText = [];
var maxTries = 100;
var curSong = null;
var model_input_batch = '0,0,0,0,0,0,0,0,0';
var model_input_weights = '0,0,0,0,0,0,1,1,1';
var current_playlist_length = 0;
var playlist_length = 2;
var availableTags = [];
var availableGenres = [];
var last_n_artists = [];
var n_artists = 3;

Array.prototype.contains = function ( needle ) {
   for (i in this) {
       if (this[i] == needle) return true;
   }
   return false;
}



function processData(data){

    all_vocab = data.split('\n');
    for (var i=0; i < all_vocab.length; i++) {
        availableTags.push(all_vocab[i].split(',')[0])
    }
}

function processGenres(data){

    all_vocab = data.split('\n');
    for (var i=0; i < all_vocab.length; i++) {
        availableGenres.push(all_vocab[i].split('|')[0])
    }
}


$(function() {
    $.ajax({
        type: "GET",
        url: "/static/data/vocab_08202014.txt",
        dataType: "text",
        success: function(data) {
            processData(data);
            $( "#word1" ).autocomplete({
                source: availableTags
            });
            $( "#word2" ).autocomplete({
                source: availableTags
            });
            $( "#word3" ).autocomplete({
                source: availableTags
            });
        }
     });

    $.ajax({
        type: "GET",
        url: "/static/data/genre_map.txt",
        dataType: "text",
        success: function(data) {
            availableGenres.push("Any")
            processGenres(data);
            var sel = document.getElementById('genres');
            for(var i = 0; i < availableGenres.length; i++) {
                var opt = document.createElement('option');
                opt.innerHTML = availableGenres[i];
                opt.value = availableGenres[i];
                sel.appendChild(opt);
            }
        }
     });

    $("#word1").focus(function() {
        // console.log('in');
    }).blur(function() {
        var word = $(this).val();
        if (availableTags.contains(word)){
            console.log('in list')
            $(this).removeClass("incorrect_tag");
        }
        else{
            console.log('out of list')
            $(this).addClass("incorrect_tag");
        }
    });




});


function sendBadTag(tag,song){
    console.log(song,tag);

}


function button_click(object,song){
    object.parent().parent().find('.badtagmenu').hide();
    var checkboxes = object.parent().find('input[name="badtag"]');

    var error_marked = 0
    checkboxes.each(function() {
        if ($(this).attr("checked")){
            error_marked = 1;
            sendBadTag(object.val(),getRdioID(song))
        }
    });

    if (error_marked == 0){
        object.parent().parent().find('.flag').attr('src','/static/assets/plus_sign.png');
    }
    else{
        object.parent().parent().find('.flag').attr('src','/static/assets/flag_red.png');
    }

}

var add_bad_tag = function (){
    $(this).parent().find('.badtagmenu').show();
};

function updateSongTable(){
    // $("#results").show();
    var rows = $("#status");
    rows.empty();
    currentSongs_length = currentSongs.length
    _.each(currentSongs, function(song,i) {
        console.log(i,currentSongs.length-1,currentSongs_length)
        if (song == curSong || i < currentSongs.length -1){
            console.log('adding button to playing song')
            var row = song.row;
            row.find(".songarea").on("click", function() {
                playSong(song);
            });
            // console.log(row.find(".update_bad_tags_button"))
            row.find(".update_bad_tags_button").on("click", function(){
                button_click($(this),song)
            });
            row.find(".flag").on("click",add_bad_tag);
        }
        else{

            var row = $("<div class='songrow'>");
            var songarea = $("<div class='songarea'>");
            var playstatus = $("<div class='playstatus'>");
            var title = $("<div class='songtitle'>");
            var badtag = $("<div class='bad_tag_button'>");

            // Add flag to bad tag menu
            var flag = $("<img class='flag' src='/static/assets/plus_sign.png' height='14' width='14'>");
            badtag.append(flag);
            // flag.hide();

            var badtagmenu = $("<div class='badtagmenu'>")
            var badtagform = $("<form action=''></form>");
            if (song.tag1){
                badtagform.append("<input type='checkbox' id='" + song.tag1.tag + i + "' name='badtag' value='" + song.tag1.tag + "'>&nbsp;<label for='" + song.tag1.tag + i + "'>" + song.tag1.tag + "</label><br>")
            }
            if (song.tag2){
                badtagform.append("<input type='checkbox' id='" + song.tag2.tag + i + "' name='badtag' value='" + song.tag2.tag + "'>&nbsp;<label for='" + song.tag2.tag + i + "'>" + song.tag2.tag + "</label><br>")
            }
            if (song.tag3){
                badtagform.append("<input type='checkbox' id='" + song.tag3.tag + i + "' name='badtag' value='" + song.tag3.tag + "'>&nbsp;<label for='" + song.tag3.tag + i + "'>" + song.tag3.tag + "</label><br>")
            }
            var button = $("<button class='update_bad_tags_button'>ok</button>")
            badtagmenu.append(badtagform)
            badtagmenu.append(button)


            badtagmenu.hide()

            if (i == 0 && curSong == null){
                playstatus.html("<button id='playFirst' class='btn btn-default pb'><i class='icon-play'></i></button>");
            }

            title.html(song.title + ' - ' + song.artist_name);
            song.row = row;
            songarea.on("click", function() {
                playSong(song);
            });

            flag.on({
                click: add_bad_tag
            });

            button.on("click", function(){
                button_click($(this),song)
            });
            song.position = i + 1
            playstatus.html(song.position);
            songarea.append(playstatus);
            songarea.append(title);
            row.append(songarea)

            badtag.append(badtagmenu)
            row.append(badtag)
            // row.append(badtagmenu)
        }



        rows.append(row);
        // rows.append('<hr class="songrowline">')

        // if (i < playlist_length - 1){
        //     // rows.append('<br>')
        // }
    });

    $("#status").fadeIn("normal", function (){});
}

function getNextSong(callback) {

    var data = {'token':$('#token').val(),'genre':$('#genres').val(),'word1':$('#word1').val(),'word2':$('#word2').val(),'word3':$('#word3').val(),'model_input_batch':model_input_batch,
    'model_input_weights':model_input_weights,'playlistKeys':playlistKeys.join(),'last_n_artists':last_n_artists.join()};

    $.post( "/refresh", data, function( response, status, xhr ) {
        if ( status == "error" ) {
            var msg = "Sorry but there was an error: ";
        $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
        }

        new_song = $.parseJSON(response)

        if (new_song['trackData'] == null){
            callback(null);
        }
        else {
            playlistText.push(new_song['trackData']['title'] + ' - ' +  new_song['trackData']['artist_name'])

            modifySongTable(new_song['trackData']);
            console.log(currentSongs.length)
            $loader.fadeOut("slow", function () {
                updateSongTable();
                callback(new_song['trackKey']);
            });


            model_input_batch = new_song['model_input_batch']
            model_input_weights = new_song['model_input_weights']

            playlistKeys.push(new_song['trackKey']);
            last_n_artists.push(new_song['trackData']['rdio-US:artistKey'])
            if (last_n_artists.length > n_artists){
                last_n_artists.shift()
            }
        }

  });
}


function startDataCollection(callback) {

    $("#results").show();
    $loader.html("Loading...")
    $loader.fadeIn("slow");

    playlist_length = $("#plength").val()

    getNextSong(function(trackKey){

        if (trackKey == null){
            $loader.html("NO MORE SONGS FOUND - TRY CHANGING TAGS/GENRE");
            $loader.fadeIn("slow");
            callback(current_playlist_length);
        }
        else{

            current_playlist_length += 1;
            if (current_playlist_length < playlist_length) {
              // console.log(trackKey);
              // console.log(current_playlist_length)
                startDataCollection(function(current_playlist_length){

                    if (current_playlist_length == playlist_length){
                        console.log('done generating...')
                    }
                    else{
                        // console.log('Fading in loader....')
                        $loader.fadeIn("slow");
                    }
                });

            }
            callback(current_playlist_length);
        }
    });
}



function error(s) {
    $("#info").addClass("alert-error");
    $("#info").removeClass("alert-success");
    $("#info").show();
    $("#info").text(s);
}

function info(s) {
    $("#info").removeClass("alert-error");
    $("#info").addClass("alert-success");
    $("#info").show();
    $("#info").text(s);
}

function clearError() {
    $("#info").hide("");
}

function getRdioID(song) {
    if (song && 'tracks' in song && song.tracks.length > 0) {
        var id = song.tracks[0].foreign_id;
        var rawID = id.split(':')[2]
        return rawID;
    } else {
        return null;
    }
}

function initUI() {
    $("#results").hide();
    $("#play-buttons").hide();

    $("#message").change(function() {
        go();
    });

    $("#save").click(function() {
        savePlaylist();
    });

    $("#go").click(function() {
        console.log('starting data collection...')
        startDataCollection(function(x){
            // console.log('onecallback'+x)
        });
    });

    $("#playNext").click(function() {
        playNextSong();
    });
    $("#playPrev").click(function() {
        playPrevSong();
    });

    $("#playPlay").click(function() {
        if (curSong == null) {
            if (currentSongs && currentSongs.length > 0) {
                playSong(currentSongs[0]);
            }
        } else {
            R.player.togglePause();
        }
    });

    $("#playPause").click(function() {
        R.player.togglePause();
    }).hide();
}


function modifySongTable(song) {
    song.prev = null;
    song.next = null;
    if (currentSongs.length > 0) {
        var last = currentSongs[currentSongs.length - 1];
        last.next = song;
        song.prev = last;
    }
    currentSongs.push(song);
    return
}

function getMatchingSong(c, genre) {
    var gcache = songCache[c, genre];
    if (c in gcache) {
        var songList = gcache[c];
        if (songList.length > 0) {
            var song = songList.shift();
            songList.push(song);
            return song;
        }
    } else {
        return getMatchingSong(c, 'rock');
    }
    return null;
}


function normalizeMessage(msg) {
    var msg_array = [];

    _.each(msg, function(c, i) {
        c = c.toLowerCase();
        if (c.match(/[a-z]/)) {
            msg_array.push(c)
        } else if (msg_array.length > 0 && msg_array[msg_array.length -1] != ' ') {
            msg_array.push(' ');
        }
    });

    return msg_array;
}


function playSong(song) {
    console.log("Playing Song.")
    console.log(song)



    if (song === curSong) {
        console.log("Toggling...")
        R.player.togglePause();
    } else {
        var oldSong = curSong;
        curSong = song;
        songChanged(oldSong);
        var id = getRdioID(curSong);
        console.log("ID",id)
        console.log(curSong)
        if (id == null) {
            playNextSong();
        } else {
            R.player.play({source: id});
        }
    }
}

function songChanged(song) {
    if (song) {
        console.log('changing class...')
        if (song === curSong && R.player.playState() === R.player.PLAYSTATE_PLAYING) {
            // $("#playPlay").remove()
            song.row.addClass('success');
            // song.row.first().children().first().html("<button id='playPause' class='btn btn-default pb'><i class='icon-pause'></i></button>");
            song.row.first().children().first().children(".playstatus").html("<button id='playPause' class='btn btn-default pb'><i class='icon-pause'></i></button>");
        } else if (song === curSong && R.player.playState() === R.player.PLAYSTATE_PAUSED){
            // song.row.first().children().first().html("<button id='playPlay' class='btn btn-default pb'><i class='icon-play'></i></button>");
            song.row.first().children().first().children(".playstatus").html("<button id='playPlay' class='btn btn-default pb'><i class='icon-play'></i></button>")
        } else {
            song.row.removeClass('success');
            song.row.first().children().first().children(".playstatus").html(song.position);
        }
    }
    else{
        songChanged(currentSongs[0]);
    }
}

function playNextSong() {
    if (curSong) {
        if (curSong.next) {
            playSong(curSong.next);
        }
    }  else {
        if (currentSongs && currentSongs.length > 0) {
            playSong(currentSongs[0]);
        }
    }
}

function playPrevSong() {
    if (curSong) {
        if (curSong.prev) {
            playSong(curSong.prev);
        }
    }  else {
        if (currentSongs && currentSongs.length > 0) {
            playSong(currentSongs[0]);
        }
    }
}

function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

function fetchGenres() {
    var predefinedGenres = ['rock', 'country', 'pop', 'jazz', 'electronic', 'classical', 'hip hop',
    'metal', 'r&b', 'blues', 'folk', 'experimental', 'alternative rock'];
    var defaultGenre = 'rock';

    predefinedGenres.sort();
    var glist = $("#genre");
    _.each(predefinedGenres, function(genre) {
        var url = '/static/js/genres/' + genre + '.js';
        $.getJSON(url, function(data) {
            songCache[genre] = {};

            _.each(data, function(song) {
                var letter = song.title[0].toLowerCase();
                if (! (letter in songCache[genre])) {
                    songCache[genre][letter] = [];
                }
                songCache[genre][letter].push(song);
            });
            _.each(songCache[genre], function(list, key) {
                shuffle(list);
            });
            var opt = $("<option>").attr('value', genre).text(genre);
            if (genre === defaultGenre) {
                opt.attr('selected', 'selected');
            }
            glist.append(opt);
        });
    });
}

function savePlaylist() {
    info("Saving the playlist");
    if (!R.authenticated()) {
        R.authenticate(
            function(state) {
                if (!state) {
                    error("Can't save the playlist unless you authorize this app");
                } else {
                    savePlaylistToRdio();
                }
            }
        );
    } else {
        savePlaylistToRdio();
    }
}

function getCurRdioTracks() {
    var ids = [];
    _.each(currentSongs, function(song) {
        var id = getRdioID(song);
        if (id) {
            ids.push(id);
        }
    });
    return ids;
}

function savePlaylistToRdio() {
    var ids = getCurRdioTracks();
    var msg = $("#message").val();
    var description = 'Created by Curato';
    var title = msg;

    if (ids.length > 0) {
        R.request(
        {
            method:"createPlaylist",
            content: {
                name: title,
                description: description,
                tracks: ids
            },

            success: function(response) {
                info("Playlist titled '" + title + "' has been saved to Rdio.");
            },

            error: function(response) {
                error("whoops - had some trouble saving that playlist to rdio");
            }
        });
    }  else {
    }
}

$(document).ready(function() {
    $loader = $("#loader")
    $.ajaxSetup( {cache: false});
    initUI();
    R.ready(
        function() {
            R.player.on("change:playingTrack", function(track) {
                if (track === null) {
                    playNextSong();
                }
            });

            R.player.on("change:playState", function(state) {

                if (state == R.player.PLAYSTATE_PAUSED) {
                    console.log("Paused!")
                    $("#playPlay").show();
                    $("#playPause").hide();
                }
                if (state == R.player.PLAYSTATE_PLAYING) {
                    console.log("Playing!")
                    $("#playPlay").hide();
                    $("#playPause").show();
                }

                if (state == R.player.PLAYSTATE_STOPPED) {
                    // playNextSong();
                }
                songChanged(curSong);
            });

            R.player.on("change:playingSource", function(track) {
                songChanged(curSong);
            });

        }
        );
    });
