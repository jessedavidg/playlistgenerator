# Playlist Generator #

Uses data from rdio playlists (250,000+) and the echonest api to train a playlist generator that takes text from a user to create a playlist. For example, "Weekend Rock and roll bbq."

## The Generator ##

1. Public playlists from rdio were used to create feature vectors for each song based on playlist titles and descriptions.
2. A neural networks was used to predict song attribute probabilities based on user input and the previous songs in a playlist.
3. A song is then chosen based on these probabilities from a database.
4. The user can view, edit, play and export the playlist in a webapp.