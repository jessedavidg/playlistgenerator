# -*- coding: utf-8 -*-

from urllib2 import HTTPError
import json, sys, random, time
from collections import defaultdict as dd
import MySQLdb, re
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
import chardet

import pylab as plt


def prep(s):
	s = s.lower()
	s = s.replace("0's","0s").replace(' n ',' and ').replace(" 'n ",' and ').replace(" 'n' ",' and ').replace(" & ",' and ')\
		.replace("'08",'2008').replace("'09",'2009').replace("'10",'2010').replace("'11",'2011').replace("'12",'2012').replace("'13",'2013')\
		.replace("'14",'2014').replace("year's",'years').replace("girl's","girls").replace("nite","night")

	return s


def tokenize_text(s):

	# print '-'*20
	# print s

	para = list()
	p = re.compile("[^0-9a-zA-Z&+]")

	for word in re.split(p,s):
		temp = word.lower().strip()
		para.append(temp)

	x = ' '.join(para)
	return x.split()



db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator",charset="utf8")
c = db.cursor()


def get_vocab():
		vocab = np.loadtxt('../../data/vocab_8_25_14.txt',delimiter='\n',dtype='str')
		vocab_map = {}
		_vocab = []
		vocab_multiple_words = []
		for k in range(len(vocab)):
			all_words = vocab[k].split(',')
			for wordI in range(len(all_words)-1):
				_vocab.append(all_words[wordI].lower())
				vocab_map[all_words[wordI].lower()] = all_words[0].lower()
				individuals = all_words[wordI].split()
				if len(individuals) > 1:
					vocab_multiple_words.append('+'.join(individuals))
		return _vocab,vocab_map,vocab_multiple_words

def get_counts(names_descriptions,vocab,total_word_counts):
	## using pre-defined vocab
	CV = CountVectorizer(vocabulary=vocab,ngram_range=(1,4),tokenizer=tokenize_text,preprocessor=prep)
	## open ended
	# CV = CountVectorizer(ngram_range=(1,4),tokenizer=tokenize_text,preprocessor=prep,stop_words=None,max_features=1200)

	names_descriptions = np.array(names_descriptions)
	fea = CV.fit_transform(names_descriptions)

	fea = fea.toarray()

	word_counts = np.sum(fea > 0,axis=0)
	word_countsI = np.argsort(word_counts)[::-1]
	for i in word_countsI:
		# print CV.get_feature_names()[i],word_counts[i]
		total_word_counts[CV.get_feature_names()[i]] += word_counts[i]
	return total_word_counts


c.execute("""SELECT name, description, playlistkey,userkey FROM playlists""")
# print len(c.fetchall())
k = 0
names_descriptions = []
playlistkeys = []

total_word_counts = dd(int)


vocab,vocab_map,vocab_multiple_words = get_vocab()

vocab = np.array(vocab)
vocab_unique = np.unique(vocab)

for v in vocab_unique:
	if np.sum(v == vocab) > 1:
		print 'DUPLICATE!!!!',v

# vocab = ['hanging with friends'	,'hang out']
# for word in vocab:
# 	total_word_counts[word] = 0


for d in c.fetchall():

	names_descriptions.append(' '.join([d[0],d[1]]))
	# names_descriptions.append(d[1])
	if k % 20000 == 0:
		print '-'*30
		print 'Fitting rows...'
		total_word_counts = get_counts(names_descriptions,vocab,total_word_counts)
		names_descriptions = []
		print 'Fit %i rows total.'%k
	k += 1
	# if k == 40000:
	# 	break


print 'Fitting rows...'
total_word_counts = get_counts(names_descriptions,vocab,total_word_counts)
print 'Fit %i rows total.'%k

aggregated_total_word_counts = {}
# aggregated_total_word_counts = total_word_counts
all_words_count = 0

## Aggregate synonyms
for word in total_word_counts:
	if not(aggregated_total_word_counts.has_key(vocab_map[word])):
		aggregated_total_word_counts[vocab_map[word]] = 0
	aggregated_total_word_counts[vocab_map[word]] += total_word_counts[word]


k = 0
word_count_list = []

for word in sorted(aggregated_total_word_counts,key=aggregated_total_word_counts.get, reverse=True):
	print '%i,%s,%i'%(k,word,aggregated_total_word_counts[word])
	all_words_count += aggregated_total_word_counts[word]
	word_count_list.append(aggregated_total_word_counts[word])
	k += 1


plt.bar(range(len(word_count_list)),word_count_list)
plt.show()

print 'Total words found:',all_words_count
