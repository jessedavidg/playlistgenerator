from rdio import Rdio
from urllib2 import HTTPError
import json, sys, random, time
from collections import defaultdict as dd
import MySQLdb

reload(sys)
sys.setdefaultencoding("utf-8")

db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator")
c = db.cursor()


# c.execute("""SELECT * FROM playlistTracks""")
# print c.fetchone()

# c.execute("""INSERT INTO playlistTracks (playlistKey, trackKey, position) VALUES (%s, %s, %s)""",
						
# 						(u'p1105443', u't21818308', 48),
# 						 )
# db.commit()
# sys.exit()


rdio = Rdio(("scprn2w4yzmac4u9duevkg6p", "vS2WyPA4fH"))

rdio.token = ('22puv67nvvxk4aswqyedu9gzdxupfesn32zsjn24g67veeq7sh9kgt23xs6psz25', 'P9A8kqxJrzTS')

all_users = json.load(open('../../data/new_users.json','rb'))
print len(all_users.keys())
print sum(all_users.values())


# for user in all_users:
# 	print user
# 	if user == 's746906':
# 		print 'Match!'
# sys.exit()

totalPlaylists = 0
userCount = 0
step = 10

getuserq = "SELECT * FROM playlists WHERE userkey=%s" 


for user in all_users:

	params = (user.encode('utf8'),)
	c.execute(getuserq, params)
	# c.execute("""SELECT * FROM playlists""")
	if c.fetchone() is not None:
		print 'Previously processed user: %s'%(user)
		continue

	offset = 0
	userCount += 1
	playlistCount = 0
	if all_users[user] == 0:
		print 'No playlists for user: %s'%(user)
		continue
	try:
		while True:
			print 'Connecting for user: %s'%(user)
			D = rdio.call('getUserPlaylists', {'user':user,'start':str(offset),'count':'10','extras':'trackKeys,description,playCount,created,lastUpdated','kind':'owned'})

			# print '-'*20
			if D.has_key('result'):
				for playlist in D['result']:
					if playlist.has_key('lastUpdated'):
						lu = playlist['lastUpdated']
					else:
						lu = None
					print playlist
					print lu
					# sys.exit()
					try:
						c.execute("""INSERT IGNORE INTO playlists (playlistKey, userKey, length, playcount, name, description, created, lastUpdated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)""",
						
						(playlist['key'], playlist['ownerKey'], playlist['length'], playlist['playCount'], playlist['name'].encode('utf8'), playlist['description'].encode('utf8'), playlist['created'], playlist['lastUpdated']),
						 )

						t = 1
						for track in playlist['trackKeys']:
							c.execute("""INSERT IGNORE INTO playlistTracks (playlistKey, trackKey, position) VALUES (%s, %s, %s)""",
							
							(playlist['key'], track, t),
							 )
							# print (playlist['key'], track, t)
							t += 1
						db.commit()
					except:
						db.rollback()
					playlistCount += 1
				if len(D['result']) < step:
					break
				offset += step
				

		# json.dump(all_users,f)
		totalPlaylists += playlistCount
		print ('User: %s, Playlists: %i, All Playlists: %i'%(userCount,playlistCount,totalPlaylists))
		print '-'*20
	except HTTPError, e:
	  # if we have a protocol error, print it
	  print e.read()
	  time.sleep(10)
