# -*- coding: utf-8 -*-

import urllib2
import json, sys, random, time
from collections import defaultdict as dd
import MySQLdb


db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator",charset="utf8")

c = db.cursor()
c2 = db.cursor()

c.execute("""SELECT DISTINCT(trackKey) FROM playlistTracks""")
# c.execute("""SELECT count(DISTINCT(trackKey)) FROM playlistTracks pt LEFT JOIN trackMap tm ON pt.trackkey = tm.rdio_US_trackid""")
# c.execute("""SELECT trackKey,count(trackkey) as c FROM playlistTracks group by trackkey order by c desc LIMIT 30""")

token = {}

toke = 1
token[1] = 'O5JN1VWQGSE6FXMN1'
token[-1] = 'P19LCHL7KMZEBMTUC'

totalSongs = 0
step = 50

current_songs = []
for song in c.fetchall():
	c2.execute("""SELECT * FROM trackMap WHERE rdio_US_trackid = %s""",
		(str(song[0]),))
	if c2.fetchone() is None:
		# print 'NEW!'
		current_songs.append(str(song[0]))
	else:
		print str(song[0])

	if (len(current_songs) == 50):
		while True:
			try:

				base_url = 'http://developer.echonest.com/api/v4/song/profile?api_key=' + token[toke] + '&bucket=id:spotify-WW&bucket=id:rdio-US&bucket=tracks'
				for key in current_songs:
					base_url += '&track_id=rdio-US:track:%s'%key
				# print base_url
				request = urllib2.Request(base_url)
				response = urllib2.urlopen(request)
				# print response.headers

				D = json.load(response)
				if not(D.has_key('response')):
					current_songs = []
					break
				if not(D['response'].has_key('songs')):
					current_songs = []
					break
				k = 0
				for temp in D['response']['songs']:
					this_song = {}
					this_song['artist'] = {}
					this_song['track'] = {}


					this_song['track']['rdio-US'] = None
					this_song['track']['spotify-WW'] = None
					this_song['track']['echonest'] = None
					this_song['artist']['rdio-US'] = None
					this_song['artist']['spotify-WW'] = None
					this_song['artist']['echonest'] = None

					# print '!'*30
					# print current_songs[k]
					k += 1
					this_song['artist']['echonest'] = temp["artist_id"]
					this_song['track']['echonest'] = temp['id']

					if not(temp.has_key('artist_foreign_ids')):
						continue

					for ids in temp["artist_foreign_ids"]:
						offset = len(ids['catalog']) + 8
						this_song['artist'][ids['catalog']] = ids['foreign_id'][offset:]
						# print '!'*30
					for temp2 in temp['tracks']:
						offset = len(temp2['catalog']) + 7
						if (temp2['catalog'] == 'rdio-US') and (temp2['foreign_id'][offset:] in current_songs):
							this_song['track']['rdio-US'] = temp2['foreign_id'][offset:]

					if this_song['track']['rdio-US'] == None:
						print 'No match found!'
						continue


					for temp2 in temp['tracks']:
						offset = len(temp2['catalog']) + 7
						if (temp2['catalog'] == 'spotify-WW'):
							this_song['track']['spotify-WW'] = temp2['foreign_id'][offset:]
							break

					# print '-'*30
					# print this_song['artist'],this_song['track']


					if this_song['artist']['rdio-US'] is not None:
						c.execute("""INSERT IGNORE INTO trackMap (rdio_US_trackid, rdio_US_artistid, echonest, spotify_WW) VALUES (%s, %s, %s,%s)""",

							(this_song['track']['rdio-US'], this_song['artist']['rdio-US'], this_song['track']['echonest'],this_song['track']['spotify-WW']),)


						c.execute("""INSERT IGNORE INTO artistMap (rdio_US_artistid, echonest, spotify_WW) VALUES (%s, %s, %s)""",

							(this_song['artist']['rdio-US'], this_song['artist']['echonest'], this_song['artist']['spotify-WW']),)

						db.commit()

					totalSongs += 1
				current_songs = []
				print 'Completed %i songs'%totalSongs
				break
			except urllib2.HTTPError, e:
			  # if we have a protocol error, print it
			  print 'Waiting...'
			  print e
			  print response.headers
			  toke = toke*-1
			  time.sleep(10)
