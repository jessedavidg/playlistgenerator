import urllib2
import json, sys, random, time
from collections import defaultdict as dd
import MySQLdb


reload(sys)
sys.setdefaultencoding("utf-8")

processed_artists = json.load(open('completed_artists.json','rb'))
# processed_artists = {}

db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator",charset="utf8")
c = db.cursor()
c2 = db.cursor()

c.execute("""SELECT DISTINCT(echonest),rdio_US_artistid FROM artistMap""")


token = {}

toke = 1
token[1] = 'O5JN1VWQGSE6FXMN1'
token[-1] = 'P19LCHL7KMZEBMTUC'

totalArtists = 0

current_genres = []
for artist in c.fetchall():
	if processed_artists.has_key(artist[1]):
		print 'Queried artist already.'
		totalArtists += 1
		continue

	# c.execute("""SELECT * FROM genres where rdio_US_artistid = %s""",(artist[1],))
	# # print c.fetchone()
	# if c.fetchone() is not None:
	# 	print 'MATCH!'
	# 	totalArtists += 1
	# 	continue
	while True:
		try:
			base_url = 'http://developer.echonest.com/api/v4/artist/profile?api_key=' + token[toke] + '&bucket=genre&bucket=artist_location' + '&id=' + artist[0]
			# base_url = 'http://developer.echonest.com/api/v4/artist/list_genres?api_key=O5JN1VWQGSE6FXMN1&format=json'
			# print base_url
			print 'Connecting for artist: %s'%artist[1]
			print base_url
			request = urllib2.Request(base_url)
			response = urllib2.urlopen(request)
			# print response.headers

			D = json.load(response)
			if not(D.has_key('response')):
				print 'No response!'
				continue

			if not(D['response']['artist'].has_key('genres')):
				print 'No genres!'
				continue
			k = 0

			for temp in D['response']['artist']['genres']:
			
				c2.execute("""INSERT IGNORE INTO genres (rdio_US_artistid, genre) VALUES (%s, %s)""",
				
					(artist[1],temp['name']))


				db.commit()

			if D['response']['artist'].has_key('name'):
				c2.execute("""UPDATE artistMap SET name = %s WHERE rdio_US_artistid = %s""",
					(D['response']['artist']['name'],artist[1]))

				db.commit()
			if D['response']['artist'].has_key('artist_location'):
				temp = D['response']['artist']['artist_location']
				for key in temp:
					# print key,temp
					c2.execute("""INSERT IGNORE INTO artistLocation (rdio_US_artistid, location_type,location) VALUES (%s, %s,%s)""",
						(artist[1],key,temp[key]))

					db.commit()
				
			totalArtists += 1
			print 'Completed %i artists'%totalArtists
			processed_artists[artist[1]] = 0
			json.dump(processed_artists,open('completed_artists2.json','wb'))

			break
		except urllib2.HTTPError, e:
			# if we have a protocol error, print it
			print 'Waiting...'
			print e
			print response.headers
			toke = toke*-1
			time.sleep(10)
		except urllib2.URLError as e:
   			print e
   			time.sleep(10)

