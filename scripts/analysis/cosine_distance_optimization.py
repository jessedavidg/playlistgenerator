# -*- coding: utf-8 -*-

from urllib2 import HTTPError
import json, sys, random, time
from collections import defaultdict as dd
import MySQLdb, re
import pylab as plt
import numpy as np
from itertools import combinations





db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator")
c = db.cursor()


query1 = """ SELECT tt.trackKey,SUM(weight*tt.tfidf)/((SQRT(SUM(POW(tt.tfidf,2)))) * .9 + 4) AS score\
			FROM `example_nn_output` AS nn,`trackTags` AS tt,\
			(SELECT DISTINCT trackKey FROM `trackTags` WHERE tid IN (%s,%s) and tfidf > 1) AS ftk\
			WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey\
			GROUP BY tt.trackKey\
			ORDER BY score DESC\
			LIMIT %s"""

query2 = """ SELECT tt.trackKey,SUM(weight*tt.tfidf)/((SQRT(SUM(POW(tt.tfidf,2)))) * .9 + 4) AS score\
			FROM `example_nn_output` AS nn,`trackTags` AS tt,\
			(SELECT DISTINCT trackKey FROM `trackTags` WHERE tid = %s and tfidf > 1) AS ftk,\
			(SELECT DISTINCT trackKey FROM `trackTags` WHERE tid = %s and tfidf > 1) AS ftk2\
			WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey AND ftk.trackKey = ftk2.trackKey\
			GROUP BY tt.trackKey\
			ORDER BY score DESC\
			LIMIT %s"""

query3 = """ SELECT tt.trackKey,SUM(weight*tt.tfidf)/((SQRT(SUM(POW(tt.tfidf,2)))) * .9 + 4) AS score\
			FROM `example_nn_output` AS nn,`trackTags` AS tt,\
			(SELECT DISTINCT trackKey FROM `trackTags` WHERE (tid = %s OR tid = %s) AND tfidf > 1) AS ftk\
			WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey
			GROUP BY tt.trackKey\
			ORDER BY score DESC\
			LIMIT %s"""

# possible_values = range(214)
# tags = random.sample(possible_values,30)
tags = range(12)
# tag = 144
# params = (','.join(all_words),10)
queries = [query2,query3]
for query in range(len(queries)):
	t = time.time()

	for tagI in range(len(tags)-1):
		print "Executing tag %i and %i..."%(tags[tagI],tags[tagI+1])

		params = (str(tags[tagI]),str(tags[tagI+1]),10)
		c.execute(queries[query],params)

	print 'Executed in %.2f seconds'%(time.time()-t)
	print '-'*40



