# -*- coding: utf-8 -*-

from rdio import Rdio
from urllib2 import HTTPError
import json, sys, random, time, csv, re
from collections import defaultdict as dd
import MySQLdb
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np

f = open('../../data/vocab_tiny.txt','r')
csv_reader = csv.reader(f)

vocab = []
vocab_map = {}


for row in csv_reader:
	vocab.append(row[0])
	if len(row) > 1:
		vocab_map[' ' + row[0] + ' '] = []
		for item in row:
			vocab_map[' ' + row[0] + ' '].append(' ' + item + ' ')


def prep(s):
	s = s.lower()
	s = s.replace("'s","s").replace(' n ',' and ').replace(" 'n ",' and ').replace(" 'n' ",' and ').replace(" & ",' and ')\
		.replace("'08",'2008').replace("'09",'2009').replace("'10",'2010').replace("'11",'2011').replace("'12",'2012').replace("'13",'2013')\
		.replace("'14",'2014').replace("year's",'years')
	# print s
	for key in vocab_map:
		for word in vocab_map[key]:
			s = s.replace(word,key)
	# print s
	# print '-'*20
	return s

def tokenize_text(s):

	# print '-'*20
	# print s

	para = list()
	p = re.compile("[^0-9a-zA-Z&+]")

	for word in re.split(p,s):
		temp = word.lower().strip()
		para.append(temp)

	x = ' '.join(para)
	return x.split()


db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator")
c = db.cursor()


c.execute("""SELECT name, description, playlistkey FROM playlists""")
# print len(c.fetchall())
k = 0
names_descriptions = []
playlistkeys = []

for d in c.fetchall():
	names_descriptions.append(' '.join([d[0],d[1]]))
	playlistkeys.append(d[2])
	# names_descriptions.append(d[1])
	k += 1
	# if k == 10000:
	# 	break

print 'Fitting...'
# print vocab
CV = CountVectorizer(vocabulary=vocab,ngram_range=(1,3),tokenizer=tokenize_text,preprocessor=prep)

fea = CV.fit_transform(names_descriptions)
fea = fea.toarray()
print fea.shape
features = np.array(CV.get_feature_names())


for tagI in range(len(vocab)):
	c.execute("""INSERT INTO tags (tid,tag) VALUES (%s, %s)""",
						
						(tagI, features[tagI]),
						 )
	db.commit()


for k in range(len(playlistkeys)):
	for tagI in range(len(features)):
		if fea[k,tagI] == 0:
			continue
		c.execute("""INSERT IGNORE INTO playlistTags (playlistKey,tid) VALUES (%s, %s)""",
						(playlistkeys[k], tagI),
						 )
		db.commit()
		# print playlistkeys[k],features[np.array(fea[k,:],dtype=bool)]
	
db.close()
# word_counts = np.sum(fea,axis=0)
# word_countsI = np.argsort(word_counts)[::-1]
# for i in word_countsI:
# 	print CV.get_feature_names()[i],word_counts[i]

sys.exit()