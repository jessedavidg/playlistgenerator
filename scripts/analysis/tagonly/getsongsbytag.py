# -*- coding: utf-8 -*-

from urllib2 import HTTPError
import json, sys, random, time
from collections import defaultdict as dd
import MySQLdb, re
import pylab as plt
import numpy as np
from itertools import combinations
from lib.validate_song import validate


def switch_vocab(vocab):
	vocabI = {}
	for k in range(len(vocab)):
		# print self.vocab[k]
		vocabI[vocab[k]] = k
	return vocabI
#
vocab = np.loadtxt('vocab250.txt',delimiter='\n',dtype='str')
vocabI = switch_vocab(vocab)

db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator")
c = db.cursor()


words = ['90s','funky']

user = 's369084'

if len(words) == 1:

	thresh = 'perc60'
	gettracks_one = """\
	SELECT ftk.trackKey,ftk.rdio_US_artistid,ftk.tfidf \
	FROM (SELECT trackKey,tfidf,rdio_US_artistid FROM tracktags_trim,trackmap,(SELECT %s from tags WHERE tid = %s) AS thresh WHERE tid = %s AND tfidf >= thresh.%s AND trackkey = rdio_us_trackid) AS ftk, \
	(SELECT trackKey FROM userfavs WHERE userkey = %s) as usr \
	WHERE usr.trackKey = ftk.trackKey \
	ORDER BY tfidf DESC \
	LIMIT 10"""%(thresh,'%s','%s',thresh,'%s')
	c.execute(gettracks_one,(vocabI[words[0]],vocabI[words[0]],user))

elif len(words) == 2:
	gettracks_one = """\
	SELECT ftk.trackKey,ftk.rdio_US_artistid,ftk.tfidf \
	FROM (SELECT trackKey,tfidf,rdio_US_artistid FROM tracktags_trim,trackmap,(SELECT perc60 from tags WHERE tid = %s) AS thresh WHERE tid = %s AND tfidf >= thresh.perc60 AND trackkey = rdio_us_trackid) AS ftk, \
	(SELECT trackKey FROM tracktags_trim,trackmap,(SELECT perc20 from tags WHERE tid = %s) AS thresh WHERE tid = %s AND tfidf >= thresh.perc20 AND trackkey = rdio_us_trackid) AS ftk2, \
	(SELECT trackKey FROM userfavs WHERE userkey = 's369084') as usr \
	WHERE usr.trackKey = ftk.trackKey and ftk.trackKey = ftk2.trackKey \
	ORDER BY tfidf DESC \
	LIMIT 10"""

	c.execute(gettracks_one,(vocabI[words[0]],vocabI[words[0]],vocabI[words[1]],vocabI[words[1]]))

for d in c.fetchall():
	print d[0],validate(d[0]),d[2]
