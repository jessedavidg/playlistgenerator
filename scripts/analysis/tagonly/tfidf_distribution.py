# -*- coding: utf-8 -*-

from urllib2 import HTTPError
import json, sys, random, time
from collections import defaultdict as dd
import MySQLdb, re
import pylab as plt
import numpy as np
from itertools import combinations





db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator")
c = db.cursor()





query = """SELECT tid,tfidf FROM tracktags_trim;"""
c.execute(query)

tfidfs = {}

for d in c.fetchall():
	tag = str(d[0])
	if not(tfidfs.has_key(tag)):
		tfidfs[tag] = []
	tfidfs[tag].append(float(d[1]))
	# tfidfs.append(float(d[1]))

all_medians = []
all_above = []
thresh = .6

plotcount = 0

for tag in tfidfs:
	plotcount += 1

	tfidfs[tag] = np.array(tfidfs[tag],dtype=float)
	tag_median = np.median(tfidfs[tag])
	perc_above = 100.*np.sum((tfidfs[tag] > thresh))/tfidfs[tag].shape[0]
	print "Percentage above %.2f for %s: %.1f%%"%(thresh,tag,perc_above)
	all_medians.append(tag_median)
	all_above.append(perc_above)

	if plotcount == 1:
		plt.figure()

	plt.subplot(4,4,plotcount)
	plt.hist(tfidfs[tag],bins=50)
	plt.xlim([0,1])
	plt.title(tag.title())

	if plotcount == 16:
		plt.show()
		plotcount = 0



plt.figure()
plt.hist(all_medians,bins=50)
plt.title("Median Distribution by Tag")

plt.figure()
plt.hist(all_above,bins=50)
plt.title("Percent above %i"%thresh)
plt.show()

# plt.xlim([0,12])
# plt.title('Tag distributions for all songs, Mean=%.2f'%(np.mean(tags_in_song)))
# plt.savefig('pivot_plots/all_songs.png')
# plt.show()
