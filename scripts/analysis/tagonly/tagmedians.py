# -*- coding: utf-8 -*-

from urllib2 import HTTPError
import json, sys, random, time
from collections import defaultdict as dd
import MySQLdb, re
import pylab as plt
import numpy as np
from itertools import combinations





db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator")
c = db.cursor()





query = """SELECT tid,tfidf FROM tracktags_trim;"""
c.execute(query)

tfidfs = {}

for d in c.fetchall():
	tag = str(d[0])
	if not(tfidfs.has_key(tag)):
		tfidfs[tag] = []
	tfidfs[tag].append(float(d[1]))
	# tfidfs.append(float(d[1]))

all_medians = []
all_above = []
thresh = .6

plotcount = 0

for tag in tfidfs:
	tfidfs[tag] = np.array(tfidfs[tag],dtype=float)
	for percentile in [20,40,60,80]:
		# tag_median = np.median(tfidfs[tag])
		print """UPDATE tags SET %s = %s WHERE tid=%s"""%('perc'+str(percentile),'%s','%s')
		query = """UPDATE tags SET %s = %s WHERE tid=%s"""%('perc'+str(percentile),'%s','%s')
		c.execute(query,(np.percentile(tfidfs[tag],percentile),tag))

db.commit()
