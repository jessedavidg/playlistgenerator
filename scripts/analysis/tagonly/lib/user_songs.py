from rdio import Rdio
import json
import MySQLdb

db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator")
c = db.cursor()


def get_favs(user,start,count):
	rdio = Rdio(("scprn2w4yzmac4u9duevkg6p", "vS2WyPA4fH"))
	rdio.token = ('22puv67nvvxk4aswqyedu9gzdxupfesn32zsjn24g67veeq7sh9kgt23xs6psz25','P9A8kqxJrzTS')

	D = rdio.call('getFavorites', {'user':user,'start':start,'type':'tracksAndAlbums','count':count})
	# try:
	# 	# print 'CAN STREAM',D['result'][songKey]['canStream']
	# 	if D['result'][songKey]['canStream'] == False:
	# 		return '__error__'
	# 	else:
	# 		return D['result'][songKey]['artist'] + ' - ' + D['result'][songKey]['name']
	# except:
	# 	print 'Song Key:',songKey
	# 	print D
	# 	return 'NO_ARTIST - ' + D['result'][songKey]['name']

	return D

if __name__ == "__main__":
	count = 50
	user = 's369084'

	for start in range(0,2000,count):
		D = get_favs(user,start,count)
		try:
			results = D["result"]
			print user,start,len(results)

			for result in results:
				if result['type'] == 'a':
					for key in result['trackKeys']:
						q = """INSERT IGNORE INTO userfavs VALUES (%s,%s)"""
						params = (user,key)
						c.execute(q,params)

				elif result['type'] == 't':
					key = result['key']
					q = """INSERT IGNORE INTO userfavs VALUES (%s,%s)"""
					params = (user,key)
					c.execute(q,params)
			db.commit()

		except:
			print D
