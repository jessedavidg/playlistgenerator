import numpy as np
import pylab as plt
import sys

f = open('../../data/train_tfidf_tiny.data','rb')

D = list()

for row in f.readlines():
	D.append(row.split())

D = np.array(D,dtype=str)

np.random.shuffle(D)
fout = open('../../data/train_tfidf_tiny_shuffle.data','wb')
for row in D:
	fout.write(' '.join(row))
	fout.write('\n')

