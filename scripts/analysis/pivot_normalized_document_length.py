# -*- coding: utf-8 -*-

from urllib2 import HTTPError
import json, sys, random, time
from collections import defaultdict as dd
import MySQLdb, re
import pylab as plt
import numpy as np
from itertools import combinations





db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator")
c = db.cursor()


# tags = [43,11,12,24,9,35,4,10,16,37,40]
tags = [43,11,12,9,10]
# tags = range(3)

a = .8
piv = 10

k = 0

if False:
	query = """SELECT q.V FROM\
				(SELECT tt.trackKey,count(tt.tcount) as length,(SQRT(SUM(POW(tt.tfidf,2)))) as V\
				FROM `example_nn_output` AS nn,`trackTags` AS tt
				WHERE nn.tagid = tt.tid
				GROUP BY tt.trackKey) AS q;"""
	c.execute(query)

	tags_in_song = []

	for d in c.fetchall():
		# print int(d[0])
		tags_in_song.append(float(d[0]))

	plt.hist(tags_in_song,bins=np.arange(0,12,.2))
	plt.xlim([0,12])
	plt.title('Tag distributions for all songs, Mean=%.2f'%(np.mean(tags_in_song)))
	plt.savefig('pivot_plots/all_songs.png')
	plt.show()

	sys.exit()

for a in [.6,.7,.8,.9]:
	for piv in [0,2,4,6,8,10]:
		tags_in_song = []
		for combination in combinations(tags,2):
			# print combination[0],combination[1]
			query = """SELECT q.V FROM\
						(SELECT tt.trackKey,count(tt.tcount) as length,(SQRT(SUM(POW(tt.tfidf,2)))) as V,SUM(weight*tt.tfidf)/((SQRT(SUM(POW(tt.tfidf,2)))) * %s + (1-%s)*%s) AS score\
						FROM `example_nn_output` AS nn,`trackTags` AS tt,\
						(SELECT DISTINCT trackKey FROM `trackTags` WHERE tid IN (%s,%s)) AS ftk\
						WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey\
						GROUP BY tt.trackKey\
						ORDER BY score DESC\
						LIMIT 100) AS q;"""

			params = (a,a,piv,combination[0],combination[1])
			c.execute(query, params)

			for d in c.fetchall():
				# print int(d[0])
				tags_in_song.append(float(d[0]))
		print a,piv,np.mean(tags_in_song)

		plt.hist(tags_in_song,bins=np.arange(0,12,.2))
		plt.xlim([0,12])
		plt.title('Tag distributions for songs selected with a=%.1f and piv = %i\n Mean=%.2f'%(a,piv,np.mean(tags_in_song)))
		plt.savefig('pivot_plots/a_%i_piv_%i.png'%(a*10,piv))
		plt.close()