import MySQLdb

db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator",charset="utf8")
c = db.cursor()

c.execute("""UPDATE trackTags AS dest,(SELECT tt.trackKey AS _trackKey,tt.tid AS _tid,tt.tcount*idf.idfvalue AS tfidf FROM `trackTags` AS tt,(SELECT tid,LN(total.track_count/COUNT(tid)) AS idfvalue FROM trackTags, (SELECT COUNT(DISTINCT trackKey) AS track_count FROM trackTags) AS total GROUP BY tid) AS idf WHERE idf.tid = tt.tid) AS src SET dest.tfidf = src.tfidf WHERE dest.trackKey=src._trackKey AND  dest.tid = src._tid;""")

db.commit()
db.close()