import MySQLdb

db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator",charset="utf8")
c = db.cursor()

c.execute("""INSERT INTO `trackTags` (trackKey, tid, tcount) SELECT B.trackKey, A.tid, COUNT(A.tid) as tcount FROM `playlistTags` as A, `playlistTracks` as B, (SELECT playlistKey FROM playlistTags GROUP BY playlistKey HAVING COUNT(playlistKey) < 4) as C WHERE  C.playlistKey = A.playlistKey AND A.playlistKey = B.playlistKey GROUP BY B.trackKey,A.tid;""")

db.commit()
db.close()