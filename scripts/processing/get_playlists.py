# -*- coding: utf-8 -*-

from rdio import Rdio
from urllib2 import HTTPError
import json, sys, random, time, datetime
from collections import defaultdict as dd
import MySQLdb

# reload(sys)
# sys.setdefaultencoding("utf-8")

db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator",charset="utf8")
c = db.cursor()


# c.execute("""SELECT * FROM playlistTracks""")
# print c.fetchone()

# c.execute("""INSERT INTO playlistTracks (playlistKey, trackKey, position) VALUES (%s, %s, %s)""",
						
# 						(u'p1105443', u't21818308', 48),
# 						 )
# db.commit()
# sys.exit()


# rdio = Rdio(("scprn2w4yzmac4u9duevkg6p", "vS2WyPA4fH"))
# rdio.token = ('22puv67nvvxk4aswqyedu9gzdxupfesn32zsjn24g67veeq7sh9kgt23xs6psz25', 'P9A8kqxJrzTS')

rdio = Rdio(("z9dju89bkx6j58txhfzdevsh", "MKwXCHts7T"))
rdio.token = ('r6tdxyv3bjmr8yx7g2m4wpwtcj8fd4setm578jryr3xm4jacqy7wpnttnpdkk4tk', 'Z95xZkuP2yGs')

## get token
# url = rdio.begin_authentication('oob')
# print('Go to: ' + url)
# verifier = input('Then enter the code: ').strip()
# rdio.complete_authentication(verifier)
# print rdio.token
# sys.exit()

all_users = json.load(open('../../data/new_users.json','rb'))
print len(all_users.keys())
print sum(all_users.values())

x = 0

for user in all_users:
	if all_users[user] != 0:
		x += 1
print 'Users with playlists:',x

# for user in all_users:
# 	print user
# 	if user == 's746906':
# 		print 'Match!'
# sys.exit()

last_update = datetime.datetime(2014,8,29,19,00,00)

totalPlaylists = 0
userCount = 0
step = 20
skippedUsers = 0

getuserq = "SELECT * FROM playlists WHERE userkey=%s" 
getplaylistq = "SELECT MAX(api_retrieval_date) FROM playlists WHERE userkey=%s"

for user in all_users:
# for user in ['s2530393']:

	api_call_count = 0

	if user in ['s771398','s5833620']:
		print 'Blacklisted user %s'%user
		continue
	# params = (user.encode('utf8'),)
	# c.execute(getuserq, params)
	# # c.execute("""SELECT * FROM playlists""")
	# if c.fetchone() is not None:
	# 	print 'Previously processed user: %s'%(user)
	# 	continue

	params = (user.encode('utf8'),)
	c.execute(getplaylistq, params)
	t = c.fetchone()[0]

	if t is not None:
		if t > last_update:
			print 'Most recent update for user %s:'%user,t
			skippedUsers += 1
			continue
		# if (datetime.datetime.now() - t).days < 0:
		# 	continue



	offset = 0
	userCount += 1
	playlistCount = 0

	if all_users[user] == 0:
		print 'No playlists for user: %s'%(user)
		continue
	try:
		while True:
			print 'Connecting for user: %s'%(user)
			D = rdio.call('getUserPlaylists', {'user':user,'start':str(offset),'count':str(step),'extras':'trackKeys,description,playCount,created,lastUpdated','kind':'owned'})

			# print '-'*20
			if D.has_key('result'):
				for playlist in D['result']:
					if playlist.has_key('lastUpdated'):
						lu = playlist['lastUpdated']
					else:
						lu = None
					# print playlist
					# print lu
					# sys.exit()
					# print playlist['name'].encode('utf8'),playlist['description'].encode('utf8')
					# print playlist['name'].decode('utf-8')
					# print playlist['description'].decode('utf-8')
					# print playlist['key'], playlist['ownerKey'], playlist['length'], playlist['playCount'], playlist['name'], playlist['description'], playlist['created']
					try:
						c.execute("""INSERT INTO playlists (playlistKey, userKey, length, playcount, name, description, created, lastUpdated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON DUPLICATE KEY UPDATE length= %s, playcount = %s, name = %s, description = %s, created = %s, lastUpdated = %s, api_retrieval_date = NOW()""",
						
						(playlist['key'], playlist['ownerKey'], playlist['length'], playlist['playCount'], playlist['name'], playlist['description'], playlist['created'], playlist['lastUpdated'], playlist['length'], playlist['playCount'], playlist['name'].encode('utf8'), playlist['description'].encode('utf8'), playlist['created'], playlist['lastUpdated']),
						 )

						t = 1
						for track in playlist['trackKeys']:


							c.execute("""INSERT INTO playlistTracks (playlistKey, trackKey, position) VALUES (%s, %s, %s) ON DUPLICATE KEY UPDATE trackKey = %s""",
							
							(playlist['key'], track, t, track),
							 )
							# print (playlist['key'], track, t)
							t += 1
						db.commit()
					except:
						print "INSERT/UPDATE ERROR"
						db.rollback()
						break

					playlistCount += 1
				if len(D['result']) < step:
					break
				offset += step
			else:
				print 'No Result for user %s!'%user
				break
				

		# json.dump(all_users,f)
		totalPlaylists += playlistCount
		print ('User: %s, Playlists: %i, All Playlists: %i, skippedUsers: %i'%(userCount,playlistCount,totalPlaylists,skippedUsers))
		print '-'*20
	except HTTPError, e:
	  # if we have a protocol error, print it
	  print e.read()
	  # sys.exit()
	  time.sleep(10)
