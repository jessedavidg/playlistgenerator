# -*- coding: utf-8 -*-

import json, sys, random, time, csv, re
from collections import defaultdict as dd
import MySQLdb
import numpy as np




db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator",charset="utf8")
c = db.cursor()
c_rdio = db.cursor()
c_rdio2 = db.cursor()
c_rdio_update = db.cursor()

get_echo_duplicates = """\
SELECT echonest \
FROM `trackMap` \
GROUP BY echonest \
HAVING count(echonest) > 1\
"""
c.execute(get_echo_duplicates)


get_rdio_duplicates = """\
SELECT tT.trackkey,SUM(tT.tcount) \
FROM (SELECT rdio_US_trackid from `trackMap` where echonest = %s)  as tM, `trackTags` as tT \
WHERE tM.rdio_US_trackid = tT.trackkey \
GROUP BY tT.trackkey \
ORDER BY SUM(tT.tcount) DESC
"""

total_songs = 0

for echonest in c.fetchall():
	params_rdio_duplicates = (echonest[0],)
	c_rdio.execute(get_rdio_duplicates,params_rdio_duplicates)
	
	k = 0
	main_track = ''

	for trackkey in c_rdio.fetchall():
		# print trackkey
		if k == 0:
			main_track = trackkey[0]
		else:
			c_rdio2.execute("""SELECT trackkey,tid,tcount FROM tracktags where trackkey = %s""",(trackkey[0],))
			for result in c_rdio2.fetchall():
				# print result
				c_rdio_update.execute("""INSERT INTO trackTags (trackkey, tid, tcount) VALUES (%s, %s, %s) ON DUPLICATE KEY UPDATE tcount = tcount + %s""",(main_track,int(result[1]),int(result[2]),int(result[2]),))
			c_rdio_update.execute("""DELETE FROM trackTags WHERE trackkey = %s""",(result[0],))
		k += 1

	print total_songs
	total_songs += 1
db.commit()
db.close()
	# sys.exit()