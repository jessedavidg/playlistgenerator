# -*- coding: utf-8 -*-

from rdio import Rdio
from urllib2 import HTTPError
import json, sys, random, time, csv, re
from collections import defaultdict as dd
import MySQLdb
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np

# f = open('../../data/vocab_tiny.txt','r')
# csv_reader = csv.reader(f)

# vocab = []
# vocab_map = {}


# for row in csv_reader:
# 	vocab.append(row[0])
# 	if len(row) > 1:
# 		vocab_map[' ' + row[0] + ' '] = []
# 		for item in row:
# 			vocab_map[' ' + row[0] + ' '].append(' ' + item + ' ')


def prep(s):
	s = s.lower()
	s = s.replace("0's","0s").replace(' n ',' and ').replace(" 'n ",' and ').replace(" 'n' ",' and ').replace(" & ",' and ')\
		.replace("'08",'2008').replace("'09",'2009').replace("'10",'2010').replace("'11",'2011').replace("'12",'2012').replace("'13",'2013')\
		.replace("'14",'2014').replace("year's",'years').replace("girl's","girls").replace("nite","night")

	return s

def tokenize_text(s):

	# print '-'*20
	# print s

	para = list()
	p = re.compile("[^0-9a-zA-Z&+]")

	for word in re.split(p,s):
		temp = word.lower().strip()
		para.append(temp)

	x = ' '.join(para)
	return x.split()


def get_vocab():
		vocab = np.loadtxt('../../data/vocab_8_25_14.txt',delimiter='\n',dtype='str')
		vocab_map = {}
		_vocab = []
		for k in range(len(vocab)):
			all_words = vocab[k].split(',')
			for wordI in range(len(all_words)-1):
				_vocab.append(all_words[wordI].lower())
				vocab_map[all_words[wordI].lower()] = {}
				vocab_map[all_words[wordI].lower()]['tag'] = all_words[0].lower()
				vocab_map[all_words[wordI].lower()]['tagid'] = k
		return _vocab,vocab_map


def write_counts(names_descriptions,playlistkeys,vocab,vocab_map):
	db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator",charset="utf8")
	c = db.cursor()

	CV = CountVectorizer(vocabulary=vocab,ngram_range=(1,4),tokenizer=tokenize_text,preprocessor=prep)

	fea = CV.fit_transform(names_descriptions)
	fea = fea.toarray()
	features = np.array(CV.get_feature_names())


	for k in range(len(playlistkeys)):
		for tagI in range(len(features)):
			if fea[k,tagI] == 0:
				continue
			c.execute("""INSERT IGNORE INTO playlistTags (playlistKey,tid) VALUES (%s, %s)""",
							(playlistkeys[k], vocab_map[features[tagI]]['tagid']),
							 )
			db.commit()
	db.close()
	return


db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator",charset="utf8")
c = db.cursor()


# print len(c.fetchall())
k = 0
names_descriptions = []
playlistkeys = []

vocab,vocab_map = get_vocab()

vocab = np.array(vocab)
vocab_unique = np.unique(vocab)

## Check for duplicates
for v in vocab_unique:
	if np.sum(v == vocab) > 1:
		print 'DUPLICATE!!!!',v

## Write tags and tagids to database
for tagI in range(len(vocab)):
	c.execute("""INSERT IGNORE INTO tags (tid,tag) VALUES (%s, %s)""",
						
						(vocab_map[vocab[tagI]]['tagid'], vocab_map[vocab[tagI]]['tag']),
						 )
	db.commit()

## get all names and descriptions
c.execute("""SELECT name, description, playlistkey FROM playlists""")

for d in c.fetchall():
	names_descriptions.append(' '.join([d[0],d[1]]))
	playlistkeys.append(d[2])
	if k % 25000 == 0:
		print '-'*30
		print 'Fitting and writing rows...'
		write_counts(names_descriptions,playlistkeys,vocab,vocab_map)
		names_descriptions = []
		playlistkeys = []
		print 'Wrote %i rows total.'%k

	k += 1

write_counts(names_descriptions,playlistkeys,vocab,vocab_map)
print 'Wrote %i rows total.'%k
	
db.close()
# word_counts = np.sum(fea,axis=0)
# word_countsI = np.argsort(word_counts)[::-1]
# for i in word_countsI:
# 	print CV.get_feature_names()[i],word_counts[i]
