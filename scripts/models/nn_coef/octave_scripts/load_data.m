function [train_input, train_target, valid_input, valid_target, test_input, test_target, vocab, ...
	train_weight_input, valid_weight_input, test_weight_input] = load_data(N)
% This method loads the training, validation and test set.
% It also divides the training set into mini-batches.
% Inputs:
%   N: Mini-batch size.
% Outputs:
%   train_input: An array of size D X N X M, where
%                 D: number of input dimensions (in this case, 3).
%                 N: size of each mini-batch (in this case, 100).
%                 M: number of minibatches.
%   train_target: An array of size 1 X N X M.
%   valid_input: An array of size D X number of points in the validation set.
%   test: An array of size D X number of points in the test set.
%   vocab: Vocabulary containing index to word mapping.


% load data.mat


all_data = dlmread('../data/train_tfidf_augmented4norm_09012014.data','');


weights = all_data(:,[10:18]);

all_data = all_data(:,[1:9,end]);


all_data = all_data + 1;

total_rows = size(all_data,1)

vocab_in = textread('../data/vocab_8_25_14.txt', '%s','delimiter','\n');


% vocab_infilename = 'csvlist.dat';
% M = csvread('../../data/vocab.txt')



n_features = size(vocab_in,1)

vocab = cell(1,n_features);


for n = 1:n_features
	% n
	% strsplit(vocab_in{n},',')(1)
	% '------------------'
	vocab{1,n} = strsplit(vocab_in{n},',')(1);
end

vocab = [vocab{:}];

trainDataLength = floor(total_rows*.8);
validDataLength = floor(total_rows*.1);

data = struct();

data.trainWeights = double(weights(1:trainDataLength,:)');
data.validWeights = double(weights(trainDataLength+1:trainDataLength+validDataLength,:)');
data.testWeights = double(weights(trainDataLength+validDataLength+1:end,:)');

data.trainData = int32(all_data(1:trainDataLength,:)');
data.validData = int32(all_data(trainDataLength+1:trainDataLength+validDataLength,:)');
data.testData = int32(all_data(trainDataLength+validDataLength+1:end,:)');
data.vocab = vocab;


numdims = size(data.trainData, 1)
D = numdims - 1;
M = floor(size(data.trainData, 2) / N);
train_input = reshape(data.trainData(1:D, 1:N * M), D, N, M);
train_target = reshape(data.trainData(D + 1, 1:N * M), 1, N, M);
valid_input = data.validData(1:D, :);
valid_target = data.validData(D + 1, :);
test_input = data.testData(1:D, :);
test_target = data.testData(D + 1, :);

train_weight_input = reshape(data.trainWeights(1:D, 1:N * M), D, N, M);

valid_weight_input = data.validWeights(1:D, :);

test_weight_input = data.testWeights(1:D, :);


vocab = data.vocab;
end
