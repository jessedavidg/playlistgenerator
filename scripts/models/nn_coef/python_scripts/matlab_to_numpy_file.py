import scipy.io
import pickle


mat = scipy.io.loadmat('../../../../data/octave/nn_07_17_14.mat')


print type(mat)

x = {}

x['word_embedding_weights'] = mat['model']['word_embedding_weights'][0][0]
x['embed_to_hid_weights'] = mat['model']['embed_to_hid_weights'][0][0]
x['hid_to_output_weights'] = mat['model']['hid_to_output_weights'][0][0]
x['hid_bias'] = mat['model']['hid_bias'][0][0]
x['output_bias'] = mat['model']['output_bias'][0][0]

pickle.dump(x,open('../../../../data/octave/model_weights.p','wb'))
