import numpy as np
import pylab as plt
import sys


f = open('../data/train_tfidf_augmented0_09012014.data','rb')


D = list()

for row in f.readlines():
	D.append(row.split())
print D[0]
D = np.array(D,dtype=float)

print D.shape
index = -7

indices = (D[:,-1] > -1)

dc = D[indices,-1]


bins = range(251)
# print np.sum(indices)/D.shape[0]
hist, bin_edges = np.histogram(dc, bins=bins)
print 'MAX:',np.max(hist)
print 'MIN:',np.min(hist)
print 'AVG:',np.mean(hist)
print 'STDV:',np.std(hist)

# bins = 50

plt.hist(dc,bins=bins,rwidth=.6,align='mid')


plt.show()
