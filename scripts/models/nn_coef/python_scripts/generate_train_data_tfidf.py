# -*- coding: utf-8 -*-


import json, sys, random, time, csv
from collections import defaultdict as dd
import MySQLdb, time, random
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np

def select_tag(counts): ## Select one song from distribution determined by 'counts'
	counts = np.array(counts)
	# clength = np.minimum(counts.shape[0],3)
	# countsI = np.argsort(counts[:,1])[::-1][:clength]

	# print counts[countsI]
	# sys.exit
	pdf = 1.0*counts[:,1]/np.sum(counts[:,1])

	# print pdf[0]

	cdf_current = 0
	p = np.random.rand()
	# print '-'*10
	for k in range(counts.shape[0]):
		cdf_current += pdf[k]
		# print k,pdf[k],cdf_current
		if p <= cdf_current:
			return str(int(counts[k,0]))
	print pdf
	print p,cdf_current,k
	print 'No tag selected!'
	sys.exit()
	return str(counts[k,0])


def position_index(position,length,n):
	if position in [1,2,3]:
		return str(n + position)
	elif (length - position) == 1:
		return str(n + 5)
	elif (length == position):
		return (str(n + 6))
	else:
		return str(n + 4)

def get_playlist_tags(tags,n_tags):
	flag = 0
	while len(tags) < 3:
		tags.append(n_tags)
		flag = 1

	if flag == 1:
		return np.array(tags,dtype=str)
	else:
		return np.array(random.sample(tags,3),dtype=str)

def get_playlist_tags_shuffled(tags):
	if len(tags) == 1:
		weights = random.sample(['1','0','0'],3)
		tags.append(tags[0])
		tags.append(tags[0])
	elif len(tags) == 2:
		weights = random.sample(['1','1','0'],3)
		if weights[0] == '0':
			tags.append(tags[0])
		else:
			tags.append(tags[1])
	else:
		tags = random.sample(tags,3)
		weights = ['1','1','1']

	return np.array(tags,dtype=str),weights

def top_track_tags(counts,n_tags):
	while len(counts) < 3:
		counts.append([counts[0][0],0])
	counts = np.array(counts,dtype=float)
	# np.random.shuffle(counts)
	# print counts
	countsI = np.argsort(counts[:,1])[::-1]
	# print countsI
	# print [counts[countsI[0],0],counts[countsI[1],0],counts[countsI[2],0]]
	# print '-'*50
	# return np.array([int(counts[countsI[0],0]),int(counts[countsI[1],0]),int(counts[countsI[2],0])],dtype=str)
	return counts[countsI[:3],:]

print 'Getting and creating dataset...'


db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator",charset="utf8")
c = db.cursor()
c2 = db.cursor()
c_albums = db.cursor()

c.execute("""SELECT p.playlistkey,length,tid FROM playlists AS p,playlistTags AS pT WHERE p.playcount > 4 AND p.length > 9 AND p.length < 51 AND pT.playlistKey  = p.playlistKey""")
# print len(c.fetchall())
playlist_count = 0
names_descriptions = []
playlistkeys = []
sequence_train_datapoints_count = 0

t = time.time()

'''
SELECT pT.position,pT.trackKey,tT.tid, tT.tcount

FROM playlists AS p, playlistTracks as pT, trackTags AS tT

WHERE p.playlistKey = %s AND p.playlistKey = pT.playlistKey AND tT.trackKey = pT.trackKey
AND (SELECT trackKey FROM trackTags WHERE trackKey = pT.trackKey GROUP BY trackKey HAVING COUNT(trackKey) > 2) = tT.trackKey

ORDER BY position ASC

'''
D = []


n_tags = 251

n_tags_str = str(n_tags)
playlist_data = {}

for d in c.fetchall():
	if not(playlist_data.has_key(d[0])):
		playlist_data[d[0]] = {}
		playlist_data[d[0]]['tags'] = []
		playlist_data[d[0]]['length'] = int(d[1])
	playlist_data[d[0]]['tags'].append(int(d[2]))


total_playlists = len(playlist_data.keys())
completed_playlists = 0
for playlistKey in playlist_data:
	print 'Completed %i of %i playlists'%(completed_playlists,total_playlists)
	completed_playlists += 1

	## Skip all playlists with more than 3 tags
	if len(playlist_data[playlistKey]['tags']) > 3:
		continue

	song_tags = dd(list)
	# q = """SELECT pT.position,pT.trackKey,tT.tid, tT.tfidf FROM playlists AS p, playlistTracks as pT, trackTags AS tT WHERE p.playlistKey = %s AND p.playlistKey = pT.playlistKey AND tT.trackKey = pT.trackKey AND (SELECT trackKey FROM trackTags WHERE trackKey = pT.trackKey GROUP BY trackKey HAVING COUNT(trackKey) > 0) = tT.trackKey ORDER BY position ASC"""
	q = """SELECT pT.position,pT.trackKey,tT.tid, tT.tfidf FROM playlists AS p, playlistTracks as pT, tracktags_trim AS tT WHERE p.playlistKey = %s AND p.playlistKey = pT.playlistKey AND tT.trackKey = pT.trackKey ORDER BY position ASC"""
	c2.execute(q,
		(playlistKey,)
		)
	## Check for same artist to remove playlists of albums
	artists_in_a_row = 5
	q_albums = """SELECT tm.rdio_US_artistid,count(tm.rdio_US_artistid) FROM playlistTracks as pT, trackMap AS tm WHERE pT.playlistKey = %s AND pT.position < %s AND tm.rdio_US_trackid = pT.trackkey GROUP BY tm.rdio_US_artistid"""
	c_albums.execute(q_albums,
		(playlistKey,(artists_in_a_row+1))
		)

	a = c_albums.fetchone()
	if a == None:
		continue
	elif int(a[1]) == artists_in_a_row:
		print 'Playlists starts with album!'
		continue


	# seq = np.zeros((playlist_data[playlistKey]['length'],))
	playlist_position = 1
	for d2 in c2.fetchall():
		# print d2
		song_tags[d2[0]].append([int(d2[2]),float(d2[3])])
		# seq[d2[0]-1] = 1
	# print song_tags


	# print song_tags
	for _dummy_ in range(2): ## create more data passes
		for trackNumber in range(1,playlist_data[playlistKey]['length']+1):
			# if not(song_tags.has_key(trackNumber) and song_tags.has_key(trackNumber-1) and song_tags.has_key(trackNumber-2)):
			# 	continue

			if not(song_tags.has_key(trackNumber)):
				continue

			output = []
			weights = []


			## Calculate tags and weights for previous two songs

			if song_tags.has_key(trackNumber-2):
				for tag,tfidf in top_track_tags(list(song_tags[trackNumber-2]),n_tags):
					# print tag,tfidf
					output.append(str(int(tag)))
					weights.append(str(tfidf))
			else:
				for _d in range(3):
					output.append("0")
					weights.append("0")

			if song_tags.has_key(trackNumber-1):
				for tag,tfidf in top_track_tags(list(song_tags[trackNumber-1]),n_tags):
					output.append(str(int(tag)))
					weights.append(str(tfidf))
			else:
				for _d in range(3):
					output.append("0")
					weights.append("0")


			## Add song tags if len(playlist_tags) < 3
			_ptags = list(playlist_data[playlistKey]['tags'])

			# while len(_ptags) < 3:
			# 	_tag = select_tag(list(song_tags[trackNumber]))
			# 	_ptags.append(_tag)



			## Get playlist tags
			playlist_tags, playlist_weights = get_playlist_tags_shuffled(_ptags)

			# print '-'*30
			# print 'number of tags:',playlist_data[playlistKey]['tags']
			# print playlist_tags,playlist_weights

			for tag,weight in zip(playlist_tags,playlist_weights):
				output.append(tag)
				weights.append(weight)

			weights.append(select_tag(list(song_tags[trackNumber])))


			sequence_train_datapoints_count += 1

			for dummy in weights:
				output.append(dummy)

			# print playlistKey,output
			D.append(output)



	# if completed_playlists > 200:

	# 	break
	playlist_count += 1
# print D
D = np.array(D,dtype=str)
np.random.shuffle(D)

print 'Writing Data...'

f = open('../data/train_tfidf_augmented0_09022014.data','wb')

Dwriter = csv.writer(f,delimiter=' ')
for row in D:
	Dwriter.writerow(row)


print 'Time taken:',time.time()-t
print 'Data Points:',sequence_train_datapoints_count
print 'Data points per playlist:',1.0*sequence_train_datapoints_count/playlist_count
