# -*- coding: utf-8 -*-

# from rdio import Rdio
from urllib2 import HTTPError
import json, sys, random, time
from collections import defaultdict as dd
import MySQLdb
from fprop import fprop
import numpy as np
import pylab as plt
from pgmodel import playlistModel


def select_song(songs,counts):
	counts = np.array(counts,dtype=float)


	pdf = 1.0*counts/np.sum(counts)

	cdf_current = 0
	p = np.random.rand()

	for k in range(counts.shape[0]):
		cdf_current += pdf[k]

		if p <= cdf_current:
			# return songs[k]
			return songs[k]
	print pdf
	print p,cdf_current,k
	print 'No tag selected!'
	sys.exit()
	return counts[0]


model = playlistModel()
print model.input_batch
print model.get_song(word1='chill',word2='folk',word3='summer')
print model.input_batch
print model.get_song(word1='rock',word2='roll',word3='party')
print model.input_batch
print model.get_song(word1='chill',word2='folk',word3='summer')
print model.input_batch
sys.exit()


vocab = np.loadtxt('../../data/vocab.txt',dtype='str')




db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator")
c = db.cursor()


# c.execute("""SELECT * FROM playlistTracks""")
# print c.fetchone()

# c.execute("""INSERT INTO playlistTracks (playlistKey, trackKey, position) VALUES (%s, %s, %s)""",
						
# 						(u'p1105443', u't21818308', 48),
# 						 )
# db.commit()
# sys.exit()
# SELECT tt.trackKey,SUM(weight*tcount) AS score, SUM(tcount)
# FROM `example_nn_output` AS nn,`trackTags` AS tt 
# WHERE nn.tagid = tt.tid
# GROUP BY tt.trackKey
# ORDER BY score DESC
# LIMIT 20;

# (SELECT DISTINCT trackKey FROM trackTags WHERE tid = 25 OR tid = 46 OR tid = 110) 6.02 s
# (SELECT DISTINCT trackKey FROM trackTags WHERE tid = 25 OR tid = 46) 5.07
# (SELECT DISTINCT trackKey FROM trackTags WHERE tid = 25) 3.42 s

# ftk: SELECT DISTINCT trackKey FROM `trackTags` WHERE tid = 10

############
'''
SELECT tt.trackKey,SUM(weight*tcount*idfvalue)/MAX(tcount) AS corr,
SUM(weight*tcount*idfvalue)/(SQRT(SUM(POW(tcount*idfvalue,2)))) AS score, count(tcount)

 FROM `example_nn_output` AS nn,`trackTags` AS tt,

 (SELECT DISTINCT trackKey FROM `trackTags` WHERE tid = 138 GROUP BY trackKey HAVING sum(tcount) > 0 ORDER BY tcount DESC LIMIT 5000) AS ftk,
 

 (SELECT tid,LN(total.pcount/COUNT(tid)) AS idfvalue
 FROM playlistTags, (SELECT COUNT(*) AS pcount FROM playlists) AS total
 GROUP BY tid) AS idf

 WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey AND idf.tid = tt.tid
 GROUP BY tt.trackKey
 ORDER BY score DESC
 LIMIT 100;
'''

############

# SELECT tid,COUNT(tid),LN(tcount/COUNT(tid)) AS idf
# FROM playlistTags, (SELECT COUNT(*) AS tcount FROM playlists) AS total
# GROUP BY tid
# ORDER BY tid ASC;

#############
startt = time.time()

input_batch = np.array([138,247,173,247,138,173,138,247,173],dtype=int)
feature_distribution = fprop(input_batch)

# for tag in input_batch:
# 	print vocab[tag]

# print '-'*20
# for k in range(9):
# 	i = np.argmax(feature_distribution)
# 	print vocab[i]
# 	feature_distribution[i] = 0

# sys.exit()

for k in range(feature_distribution.shape[0]):
	getuserq = "REPLACE INTO `playlist_generator`.`example_nn_output` (`nnid`, `tagid`, `weight`) VALUES ('2', %s, %s);" 
	params = (k,feature_distribution[k][0])
	c.execute(getuserq, params)

db.commit()

print 'Time to insert new weights: %.2f'%(time.time() - startt)
t2 = time.time()
print 'Finding matching songs...'

gettracksq = """\
SELECT tt.trackKey,SUM(weight*tcount*idfvalue)/(SQRT(SUM(POW(tcount*idfvalue,2)))) AS score\
 FROM `example_nn_output` AS nn,`trackTags` AS tt,\
 (SELECT DISTINCT trackKey FROM `trackTags` WHERE tid = %s ORDER BY tcount DESC LIMIT 5000) AS ftk,\
 (SELECT tid,LN(total.pcount/COUNT(tid)) AS idfvalue\
 FROM playlistTags, (SELECT COUNT(*) AS pcount FROM playlists) AS total\
 GROUP BY tid) AS idf\
 WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey AND idf.tid = tt.tid\
 GROUP BY tt.trackKey\
 ORDER BY score DESC\
 LIMIT 100"""

# print gettracksq
print np.argmax(feature_distribution)
params = (np.argmax(feature_distribution),)


c.execute(gettracksq, params)


print 'Time to match songs: %.2f'%(time.time() - t2)

song_list = []
score_list = []

for d in c.fetchall():
	song_list.append(d[0])
	score_list.append(d[1])
	print d[0],d[1]

'''
t = np.zeros((len(song_list),1))
print 'Simulating...'
for k in range(1000000):
	j = select_song(song_list,score_list)
	t[j] += 1

plt.figure(1)

plt.bar(range(len(score_list)),score_list)
plt.figure(2)
# plt.ylim((1.5,2))
plt.bar(range(len(score_list)),t)
plt.show()
'''

print select_song(song_list,score_list)

print 'Total time: %.2f'%(time.time() - startt)

