import numpy as np
import scipy.io
import sys
# function [embedding_layer_state, hidden_layer_state, output_layer_state] = ...
#   fprop(input_batch, word_embedding_weights, embed_to_hid_weights,...
#   hid_to_output_weights, hid_bias, output_bias)
# This method forward propagates through a neural network.
# Inputs:
#   input_batch: The input data as a matrix of size numwords X batchsize where,
#     numwords is the number of words, batchsize is the number of data points.
#     So, if input_batch(i, j) = k then the ith word in data point j is word
#     index k of the vocabulary.
#
#   word_embedding_weights: Word embedding as a matrix of size
#     vocab_size X numhid1, where vocab_size is the size of the vocabulary
#     numhid1 is the dimensionality of the embedding space.
#
#   embed_to_hid_weights: Weights between the word embedding layer and hidden
#     layer as a matrix of soze numhid1*numwords X numhid2, numhid2 is the
#     number of hidden units.
#
#   hid_to_output_weights: Weights between the hidden layer and output softmax
#               unit as a matrix of size numhid2 X vocab_size
#
#   hid_bias: Bias of the hidden layer as a matrix of size numhid2 X 1.
#
#   output_bias: Bias of the output layer as a matrix of size vocab_size X 1.
#
# Outputs:
#   embedding_layer_state: State of units in the embedding layer as a matrix of
#     size numhid1*numwords X batchsize
#
#   hidden_layer_state: State of units in the hidden layer as a matrix of size
#     numhid2 X batchsize
#
#   output_layer_state: State of units in the output layer as a matrix of size
#     vocab_size X batchsize
#
def fprop(input_batch):

	mat = scipy.io.loadmat('../nn_python/nn.mat')

	word_embedding_weights = mat['word_embedding_weights']
	embed_to_hid_weights = mat['embed_to_hid_weights']
	hid_to_output_weights = mat['hid_to_output_weights']
	hid_bias = mat['hid_bias']
	output_bias = mat['output_bias']
	
	numwords = len(input_batch)
	batchsize = 1

	vocab_size, numhid1 = word_embedding_weights.shape
	numhid2 = embed_to_hid_weights.shape[1]


	## COMPUTE STATE OF WORD EMBEDDING LAYER.
	# Look up the inputs word indices in the word_embedding_weights matrix.
	# embedding_layer_state = reshape(...
	#   word_embedding_weights(reshape(input_batch, 1, []),:)',...

	embedding_layer_state = word_embedding_weights[input_batch,:].transpose().reshape(numhid1 * numwords, 1, order='F').copy()



	## COMPUTE STATE OF HIDDEN LAYER.
	# Compute inputs to hidden units.
	# print embed_to_hid_weights.transpose().shape
	# print embedding_layer_state.shape

	# print np.dot(embed_to_hid_weights.transpose(),embedding_layer_state).shape

	inputs_to_hidden_units = np.dot(embed_to_hid_weights.transpose(),embedding_layer_state) + np.tile(hid_bias, (1, batchsize))

	# Apply logistic activation function.


	hidden_layer_state = 1. / (1 + np.exp(-inputs_to_hidden_units));
	# print hidden_layer_state
	# sys.exit()
	## COMPUTE STATE OF OUTPUT LAYER.
	# Compute inputs to softmax.

	inputs_to_softmax = np.dot(hid_to_output_weights.transpose(),hidden_layer_state) +  np.tile(output_bias, (1, batchsize))



	# Subtract maximum. 
	# Remember that adding or subtracting the same constant from each input to a
	# softmax unit does not affect the outputs. Here we are subtracting maximum to
	# make all inputs <= 0. This prevents overflows when computing their
	# exponents.
	inputs_to_softmax = inputs_to_softmax - np.tile(max(inputs_to_softmax), (vocab_size, 1));

	# Compute exp.
	output_layer_state = np.exp(inputs_to_softmax);
	output_layer_state.shape
	# print np.tile(np.sum(output_layer_state, 0), (vocab_size, 1)).shape
	# sys.exit()
	# Normalize to get probability distribution.
	output_layer_state = output_layer_state / np.sqrt(np.sum(np.power(output_layer_state,2)))
	# output_layer_state = output_layer_state / np.tile(np.sum(output_layer_state, 0), (vocab_size, 1));

	return output_layer_state

if __name__ == "__main__":
	print 'x'
	input_batch = np.array([138,247,138,247,138,247,138,247,138],dtype=int)
	fprop(input_batch)

