import webapp2
from webapp2_extras import jinja2
from google.appengine.api import urlfetch
import logging,datetime
import urllib, time
import urllib2
import json,os
import numpy as np
from pgmodel import playlistModel
from rdio import Rdio

class BaseHandler(webapp2.RequestHandler):

    @webapp2.cached_property
    def jinja2(self):
        # Returns a Jinja2 renderer cached in the app registry.
        return jinja2.get_jinja2(app=self.app)

    def render_response(self, _template, **context):
        # Renders a template and writes the result to the response.
        rv = self.jinja2.render_template(_template, **context)
        self.response.write(rv)


class IndexHandler(BaseHandler):

    # model = playlistModel()

    def get(self):
        print 'started...'
        # if not(self.request.get('code')):
        #     dialog_url = login_fb.start_dialog(clientID)
        #     self.redirect(dialog_url)
        #     context = {'oldguess':'didnotwork'}
        # else:
        #     access_token = login_fb.get_token(self.request.get('code'),clientID)
        #     sdate,edate = self.default_dates()
        rdio = Rdio(("scprn2w4yzmac4u9duevkg6p", "vS2WyPA4fH"))
        rdio.token = ('22puv67nvvxk4aswqyedu9gzdxupfesn32zsjn24g67veeq7sh9kgt23xs6psz25','P9A8kqxJrzTS')

        context = {'word1':'relax','word2':'','word3':'','token':rdio.token}
        self.render_response('curato.html', **context)

    def post(self):
        word1 = self.request.get('word1')
        word2 = self.request.get('word2')
        word3 = self.request.get('word3')

        context = {'word1':word1,'word2':word2,'word3':word3,'data':'','trackid':self.model.get_song(word1=word1,word2=word2,word3=word3)}
        self.render_response('curato.html',**context)


class UpdateDataHandler(BaseHandler):

    model = playlistModel()
    

    def get(self):

        # print 'word1:',word1
        # trackKey = self.model.get_song(word1=word1,word2=word2,word3=word3)
        # path = os.path.join(os.path.dirname(__file__), 'index.html')
        self.response.out.write('bluff')

    def post(self):
        print 'post...'
        data = {}

        

        word1 = self.request.get('word1')
        word2 = self.request.get('word2')
        word3 = self.request.get('word3')
        token = self.request.get('token')
        playlistKeys = self.request.get('playlistKeys')


        model_input_batch = self.request.get('model_input_batch')
        model_input_weights = self.request.get('model_input_weights')
        # print 'model_input_weights:',model_input_weights,type(model_input_weights),model_input_weights[0],model_input_weights[-1]
        # print 'model_input_batch:',model_input_batch,type(model_input_batch)
        # print 'Token!',token
        # playlist = eval(self.request.get('playlist'))
        # print 'playlist value:',playlist
        trackKey,trackData,model_input_batch,model_input_weights = self.model.get_song(playlist=playlistKeys,input_batch=model_input_batch,input_weights=model_input_weights,word1=word1,word2=word2,word3=word3)
        # D = 
        # playlist.append(D)

        data['trackData'] = trackData
        data['trackKey'] = trackKey

        if word1 == '':
            data['trackData']['tag1'] = None
        else:
            data['trackData']['tag1'] = {}
            data['trackData']['tag1']['tag'] = word1
            data['trackData']['tag1']['marked'] = 0
        if word2 == '':
            data['trackData']['tag2'] = None
        else:
            data['trackData']['tag2'] = {}
            data['trackData']['tag2']['tag'] = word2
            data['trackData']['tag2']['marked'] = 0
        if word3 == '':
            data['trackData']['tag3'] = None
        else:
            data['trackData']['tag3'] = {}
            data['trackData']['tag3']['tag'] = word3
            data['trackData']['tag3']['marked'] = 0

        data['model_input_batch'] = model_input_batch
        data['model_input_weights'] = model_input_weights

        self.response.out.write(json.dumps(data))

class HelperHandler(BaseHandler):

    def get(self):
        print 'Get helper'
        self.request.get('word1')
        context = {'':''}
        self.render_response('helper.html', **context)

    def post(self):
        print 'Post helper'
        context = {'':''}
        self.render_response('_helper.html',**context)
