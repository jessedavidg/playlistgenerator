import numpy as np

import MySQLdb
import pickle,random
from validate_song import validate

class playlistModel:
	# db = MySQLdb.connect(unix_socket='/cloudsql/playlist-generator:mixtape', db='mixtape', user='root')
	# db = MySQLdb.connect("173.194.248.232","root","m!xt@pe","mixtape")
	# db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator")

	def __init__(self):
		mat = pickle.load(open('model_weights.p','rb'))

		self.word_embedding_weights = mat['word_embedding_weights']
		self.embed_to_hid_weights = mat['embed_to_hid_weights']
		self.hid_to_output_weights = mat['hid_to_output_weights']
		self.hid_bias = mat['hid_bias']
		self.output_bias = mat['output_bias']


		self.vocab = np.loadtxt('vocab_07112014.txt',delimiter='\n',dtype='str')
		self.vocab_is_genre = {}

		for k in range(len(self.vocab)):
			_temp = self.vocab[k].split(',')
			self.vocab[k] = _temp[0]
			self.vocab_is_genre[self.vocab[k]] = bool(int(_temp[-1]))
		self.vocabI = self.switch_vocab()



	def fprop(self,input_batch,input_weights):
		print 'fprop'
		numwords = len(input_batch)
		batchsize = 1

		vocab_size, numhid1 = self.word_embedding_weights.shape
		numhid2 = self.embed_to_hid_weights.shape[1]



		## COMPUTE STATE OF WORD EMBEDDING LAYER.
		# Look up the inputs word indices in the word_embedding_weights matrix.

		# print '-'*20
		# print self.word_embedding_weights.shape
		# print '-'*20

		'''
		# No input weights for tags
		embedding_layer_state = self.word_embedding_weights[input_batch,:].transpose().reshape(numhid1 * numwords, 1, order='F').copy()
		'''

		# Include input weights for tags
		embedding_layer_state = (self.word_embedding_weights[input_batch,:].transpose() * np.tile(input_weights,(numhid1,1))).reshape(numhid1 * numwords, 1, order='F').copy()


		## COMPUTE STATE OF HIDDEN LAYER.
		# Compute inputs to hidden units.


		inputs_to_hidden_units = np.dot(self.embed_to_hid_weights.transpose(),embedding_layer_state) + np.tile(self.hid_bias, (1, batchsize))

		# Apply logistic activation function.

		hidden_layer_state = 1. / (1 + np.exp(-inputs_to_hidden_units));

		## COMPUTE STATE OF OUTPUT LAYER.
		# Compute inputs to softmax.


		inputs_to_softmax = np.dot(self.hid_to_output_weights.transpose(),hidden_layer_state) +  np.tile(self.output_bias, (1, batchsize))


		# Subtract maximum.
		inputs_to_softmax = inputs_to_softmax - np.tile(max(inputs_to_softmax), (vocab_size, 1));

		# Compute exp.
		output_layer_state = np.exp(inputs_to_softmax);

		# output_layer_state = output_layer_state / np.sqrt(np.sum(np.power(output_layer_state,2))) # Normalize sum of squares to 1
		output_layer_state = output_layer_state / np.sum(output_layer_state) # Normalize sum  to 1

		return output_layer_state

	def get_song(self,playlist,input_batch,input_weights,word1,word2=None,word3=None):

		db = MySQLdb.connect("localhost","root","purplepants123","playlist_generator")
		# db = MySQLdb.connect(unix_socket='/cloudsql/playlist-generator:mixtape', db='mixtape', user='root')

		playlist = playlist.split(',')
		position = len(playlist)
		print playlist
		print 'Playlist:',playlist,type(playlist),position

		## Model inputs
		input_batch = np.array(input_batch.split(','),dtype=int)
		input_weights = np.array(input_weights.split(','),dtype=float)

		## Selection requirements
		required_words = []
		influential_words = []
		all_words = []

		all_words.append(str(self.vocabI[word1]))
		if self.vocab_is_genre[word1]:
			required_words.append(str(self.vocabI[word1]))
		else:
			influential_words.append(str(self.vocabI[word1]))

		input_batch[-3] = self.vocabI[word1]
		if word2 != '':
			input_batch[-2] = self.vocabI[word2]
			input_weights[-2] = 1
			all_words.append(str(self.vocabI[word2]))
			if self.vocab_is_genre[word2]:
				required_words.append(str(self.vocabI[word2]))
			else:
				influential_words.append(str(self.vocabI[word2]))
		else:
			input_weights[-2] = 0
		if word3 != '':
			input_batch[-1] = self.vocabI[word3]
			input_weights[-1] = 1
			all_words.append(str(self.vocabI[word3]))
			if self.vocab_is_genre[word3]:
				required_words.append(str(self.vocabI[word3]))
			else:
				influential_words.append(str(self.vocabI[word3]))
		else:
			input_weights[-1] = 0



		## Forward propagate neural network and store output in database
		feature_distribution = self.fprop(input_batch,input_weights)
		c = db.cursor()

		for k in range(feature_distribution.shape[0]):
			getuserq = "REPLACE INTO `example_nn_output` (`nnid`, `tagid`, `weight`) VALUES ('2', %s, %s);"
			params = (k,feature_distribution[k][0])
			c.execute(getuserq, params)

		db.commit()

		if playlist[0] == '':
			lim = 5
		else:
			lim = 100


		# ## cosine similarity
		if required_words and False:
			print "new query!!!"
			gettracksq = """\
			SELECT tt.trackKey,SUM(weight*tt.tfidf)/((SQRT(SUM(POW(tt.tfidf,2)))) * .9 + 4) AS score\
			FROM `example_nn_output` AS nn,`trackTags` AS tt,\
			(SELECT DISTINCT trackKey FROM `trackTags` WHERE tid IN (%s) GROUP BY trackKey HAVING COUNT(trackKey) = %s) AS ftk\
			WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey\
			GROUP BY tt.trackKey\
			ORDER BY score DESC\
			LIMIT %s"""%(','.join(['%s'] * len(required_words)),'%s','%s')

			_params = list(required_words)
			_params.append(len(required_words))
			_params.append(lim)

			params = tuple(_params)

		else:
			print "original query!!!!!!"
			gettracksq = """\
			SELECT tt.trackKey,SUM(weight*tt.tfidf)/((SQRT(SUM(POW(tt.tfidf,2)))) * .9 + 4) AS score\
			FROM `example_nn_output` AS nn,`trackTags` AS tt,\
			(SELECT DISTINCT trackKey FROM `trackTags` WHERE tid IN (%s)) AS ftk\
			WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey\
			GROUP BY tt.trackKey\
			ORDER BY score DESC\
			LIMIT %s"""

			params = (','.join(all_words),lim)


		'''
		*** Required ***

		SELECT tt.trackKey,SUM(tt.tcount),SUM(weight*tt.tfidf)/((SQRT(SUM(POW(tt.tfidf,2))))) AS score
		FROM `example_nn_output` AS nn,`trackTags` AS tt,
		(SELECT DISTINCT trackKey FROM `trackTags` WHERE tid IN (52) GROUP BY trackKey HAVING COUNT(trackKey) = 1 AND MIN(tcount) > 1) AS ftk
		WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey
		GROUP BY tt.trackKey
		ORDER BY score DESC
		LIMIT 25;


		*** Influencers ***

		SELECT tt.trackKey,SUM(tt.tcount),count(tt.tcount),SUM(weight*tt.tfidf)/((SQRT(SUM(POW(tt.tfidf,2))))) AS score
		FROM `example_nn_output` AS nn,`trackTags` AS tt,
		(SELECT DISTINCT trackKey FROM `trackTags` WHERE tid IN (101)) AS ftk
		WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey
		GROUP BY tt.trackKey
		ORDER BY score DESC
		LIMIT 25;

		Correction for cosine similarity relevance: V(d) -> a * V(d) + (1 - a) * piv
		piv = 20...

		SELECT AVG(q.length) FROM
		(SELECT tt.trackKey,SUM(tt.tcount) as length,SUM(weight*tt.tfidf)/((SQRT(SUM(POW(tt.tfidf,2)))) * .8 + (1-.8)*10) AS score
		FROM `example_nn_output` AS nn,`trackTags` AS tt,
		(SELECT DISTINCT trackKey FROM `trackTags` WHERE tid IN (28,30,25) AS ftk
		WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey
		GROUP BY tt.trackKey
		ORDER BY score DESC
		LIMIT 50) AS q;
		'''



		# cross entropy
		# gettracksq = """
		# SELECT tt.trackKey,(16*(1 - SUM(nn.weight)) - SUM(weight*(LN(tt.tfidf_norm)))) AS score
		# FROM `example_nn_output` AS nn,`trackTags` AS tt,
		# (SELECT DISTINCT trackKey FROM `trackTags` WHERE tid = %s order by tfidf LIMIT 20000) AS ftk
		# WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey
		# GROUP BY tt.trackKey
		# ORDER BY score ASC
		# LIMIT %s"""

		'''
		SELECT tt.trackKey,sum(tt.tcount) as tid_count,16*(1 - SUM(nn.weight)),-SUM(weight*(LN(tt.tfidf_norm))),(16*(1 - SUM(nn.weight)) - SUM(weight*(LN(tt.tfidf_norm)))) AS score
		FROM `example_nn_output` AS nn,`trackTags` AS tt,
		(SELECT DISTINCT trackKey FROM `trackTags` WHERE tid = 31 order by tfidf LIMIT 20000) AS ftk
		WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey
		GROUP BY tt.trackKey
		ORDER BY score ASC
		LIMIT 100
		'''


		'''
		SELECT MAX(CE.tid_count),MIN(CE.tid_count),AVG(CE.tid_count),STD(CE.tid_count) FROM
		(SELECT tt.trackKey,n.norm as norm, sum(tt.tcount) as tid_count,8*(1 - SUM(nn.weight)),-SUM(weight*(LN(tt.tfidf/n.norm))),(8*(1 - SUM(nn.weight)) - SUM(weight*(LN(tt.tfidf/n.norm)))) AS score
		FROM `example_nn_output` AS nn,`trackTags` AS tt,
		(SELECT DISTINCT trackKey FROM `trackTags` WHERE tid = 30 order by tfidf LIMIT 20000) AS ftk,
		(SELECT trackKey,SUM(tfidf) as norm FROM `trackTags` GROUP BY trackKey) AS n
		WHERE nn.tagid = tt.tid AND ftk.trackKey = tt.trackKey AND ftk.trackKey = n.trackKey
		GROUP BY tt.trackKey
		ORDER BY score ASC
		LIMIT 50) AS CE;
		'''


		# params = (np.argmax(feature_distribution),)



		print 'params:',params

		c.execute(gettracksq, params)

		song_list = []
		score_list = []


		for d in c.fetchall():
			song_list.append(d[0])
			score_list.append(d[1])


		# song_selection_successful =  0

		while True:
			selected_song = self.select_song(song_list,score_list)
			if selected_song in playlist:
				continue
			song_data = validate(selected_song)
			if song_data != '__error__':
				break


		print 'selected song:',selected_song
		gettids = """SELECT tid,tfidf FROM `trackTags` WHERE trackKey = %s ORDER BY tfidf DESC LIMIT 3"""
		params = (selected_song,)
		c.execute(gettids, params)

		## Print


		## update input batch

		print '------start input batch-----'
		print input_batch
		print input_weights
		print '-----------------------'

		input_batch[0:3] = input_batch[3:6]
		input_weights[0:3] = input_weights[3:6]


		input_weights[3:6] = 0
		index = 3
		for d in c.fetchall():
			input_weights[index] = float(d[1])
			input_batch[index] = int(d[0])
			index += 1
			print d



		print '------updated input batch for next song-----'
		print input_batch
		print input_weights
		print '------------------------'



		gettids = """SELECT count(tid) FROM `trackTags` WHERE trackKey = %s"""
		params = (selected_song,)
		c.execute(gettids, params)
		print 'Total tags for chosen song:',int(c.fetchone()[0])


		db.close()
		return selected_song,song_data,','.join(input_batch.astype(str)),','.join(input_weights.astype(str))

	def select_song(self,songs,counts):

		return random.sample(songs,1)[0]


		counts = np.array(counts,dtype=float)
		print 'Length of results:',len(songs)


		pdf = 1.0*counts/np.sum(counts)

		cdf_current = 0
		p = np.random.rand()

		for k in range(counts.shape[0]):
			cdf_current += pdf[k]

			if p <= cdf_current:
				# return songs[k]
				return songs[k]
		print pdf
		print p,cdf_current,k
		print 'No tag selected!'
		sys.exit()
		return counts[0]

	def switch_vocab(self):
		vocabI = {}
		for k in range(len(self.vocab)):
			vocabI[self.vocab[k]] = k
		return vocabI
